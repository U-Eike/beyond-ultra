import sys
import os
from pathlib import Path
import json
import math
from openpyxl import Workbook


os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Itemdex = os.path.join(Out_Files, "ItemEN.json")
#Weapon Out
WDP = os.path.join(Remapped_Data, "WeaponData.json")
WPDP = os.path.join(Remapped_Data, "WeaponPromoteData.json")
#Character Out
ADP = os.path.join(Remapped_Data, "AvatarData.json")
ASDDP = os.path.join(Remapped_Data, "AvatarSkillDepotData.json")
ASDP = os.path.join(Remapped_Data, "AvatarSkillData.json")
ATDP = os.path.join(Remapped_Data, "AvatarTalentData.json")
FSDP = os.path.join(Remapped_Data, "FetterStoryData.json")
FIDP = os.path.join(Remapped_Data, "FetterInfoData.json")
PSDP = os.path.join(Remapped_Data, "ProudSkillData.json")
APDP = os.path.join(Remapped_Data, "AvatarPromoteData.json")
ACDP = os.path.join(Remapped_Data, "AvatarCurveData.json")
MTMP = os.path.join(Remapped_Data, "ManualTextMap.json")
RDP = os.path.join(Remapped_Data, "ReliquaryData.json")
EADP = os.path.join(Remapped_Data, "EquipAffixData.json")
RSDP = os.path.join(Remapped_Data, "ReliquarySetData.json")
WCD = os.path.join(Remapped_Data, "WeaponCurveData.json")
MDP = os.path.join(Remapped_Data, "MaterialData.json")
FDP = os.path.join(Remapped_Data, "ForgeData.json")
CDP = os.path.join(Remapped_Data, "CombineData.json")
CRDP = os.path.join(Remapped_Data, "CookRecipeData.json")
CBDP = os.path.join(Remapped_Data, "CookBonusData.json")
IMDP = os.path.join(Remapped_Data, "InvestigationMonster.json")
MSDDP = os.path.join(Remapped_Data, "MaterialSourceDataData.json")
RPDP = os.path.join(Remapped_Data, "RewardPreviewData.json")
DIDP = os.path.join(Remapped_Data, "DisplayItemData.json")
MBDP = os.path.join(Remapped_Data, "MechanicBuildingData.json")
GDP = os.path.join(Remapped_Data, "GadgetData.json")
MGLUDP = os.path.join(Remapped_Data, "MechanicusGearLevelUpData.json")
QDP = os.path.join(Remapped_Data, "QuestData.json")
MQDP = os.path.join(Remapped_Data, "MainQuestData.json")
TDP =  os.path.join(Remapped_Data, "TalkData.json")
DDP =  os.path.join(Remapped_Data, "DialogueData.json")
DDP =  os.path.join(Remapped_Data, "DialogueData.json")
NDP =  os.path.join(Remapped_Data, "NPCData.json")
QCDP =  os.path.join(Remapped_Data, "QuestCodexData.json")
QChDP =  os.path.join(Remapped_Data, "QuestChapterData.json")

#MechanicusGearLevelUpExcelConfigData.json.json

def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret

def Baseloader(Loc):
    Loc = os.path.join(Base_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

ItemEN = Outloader(Itemdex)

def WeaponPromoteData():

    WPECD = Baseloader("WeaponPromoteExcelConfigData.json")

    final = {}
    for item in WPECD:
        if item["WeaponPromoteId"] not in final:
            temp = {}
            temp.update({0 : {}})
            temp[0].update({"UnlockMaxLevel" : item["UnlockMaxLevel"]})
            final.update({item["WeaponPromoteId"] : temp})

        else:
            temp = {}
            temp.update({item["PromoteLevel"] : {}})
            Evol = {"Mora" : [],"Mats" : []}
            try:
                Evol.update({"Mora" : [item["CoinCost"]] })
            except:
                Evol.update({"Mora" : [0] })
            try:
                for thing in item["CostItems"]:
                    Idd = str(thing["Id"])
                    #T2 = []
                    T2 = {
                        "Id" : ItemEN[Idd],
                        "Count" : thing["Count"]
                    }
                    Evol["Mats"].append(T2)
            except:
                Evol.update({"Mats" : [] })

            temp[item["PromoteLevel"]].update({ "Evolution" : Evol})
            temp[item["PromoteLevel"]].update({"UnlockMaxLevel" : item["UnlockMaxLevel"]})
            temp[item["PromoteLevel"]].update({"Value" : item["AddProps"][0]["Value"]})
            final[item["WeaponPromoteId"]].update(temp)

    with open(WPDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def WeaponData():
    WECD = Baseloader("WeaponExcelConfigData.json")

    final = {}
    for item in WECD:
        loc = WECD.index(item)
        final.update({WECD[loc]["Id"] : item})
    with open(WDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def AvatarData():
    AECD = Baseloader("AvatarExcelConfigData.json")
    final = {}
    for item in AECD:
        loc = AECD.index(item)
        final.update({AECD[loc]["Id"] : item})
    with open(ADP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def CombineData():
    CECD = Baseloader("CombineExcelConfigData.json")
    final = {}
    for item in CECD:
        loc = CECD.index(item)
        final.update({CECD[loc]["CombineId"] : item})
    with open(CDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def CookBonusData():
    CBECD = Baseloader("CookBonusExcelConfigData.json")
    final = {}
    for item in CBECD:
        loc = CBECD.index(item)
        final.update({CBECD[loc]["RecipeId"] : item})
    with open(CBDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def CookRecipeData():
    CRECD = Baseloader("CookRecipeExcelConfigData.json")
    final = {}
    for item in CRECD:
        loc = CRECD.index(item)
        final.update({CRECD[loc]["Id"] : item})
    with open(CRDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ForgeData():
    FECD = Baseloader("CombineExcelConfigData.json")
    final = {}
    for item in FECD:
        loc = FECD.index(item)
        final.update({FECD[loc]["ResultItemId"] : item})
    with open(FDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def AvatarSkillData():
    ASECD = Baseloader("AvatarSkillExcelConfigData.json")
    final = {}
    for item in ASECD:
        loc = ASECD.index(item)
        final.update({ASECD[loc]["Id"] : item})
    with open(ASDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def AvatarSkillDepotData():
    ASDEC = Baseloader("AvatarSkillDepotExcelConfigData.json")
    final = {}
    final = {}
    for item in ASDEC:
        loc = ASDEC.index(item)
        final.update({ASDEC[loc]["Id"] : item})
    with open(ASDDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def AvatarTalentData():
    ATECD = Baseloader("AvatarTalentExcelConfigData.json")
    final = {}
    for item in ATECD:
        loc = ATECD.index(item)
        final.update({ATECD[loc]["TalentId"] : item})
    with open(ATDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def FetterStoryData():
    FSECD = Baseloader("FetterStoryExcelConfigData.json")
    final = {}
    for item in FSECD:
        loc = FSECD.index(item)
        if FSECD[loc]["AvatarId"] not in final:
            temp = {}
            temp.update({"ID" : FSECD[loc]["FetterId"]})
            temp.update({"Name" : FSECD[loc]["StoryTitleTextMapHash"]})
            temp.update({"Description" : FSECD[loc]["StoryContextTextMapHash"]})
            final.update({FSECD[loc]["AvatarId"] : [temp]})

        else:
            temp = {}
            temp.update({"ID" : FSECD[loc]["FetterId"]})
            temp.update({"Name" : FSECD[loc]["StoryTitleTextMapHash"]})
            temp.update({"Description" : FSECD[loc]["StoryContextTextMapHash"]})
            final[FSECD[loc]["AvatarId"]].append(temp)
    with open(FSDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def FetterInfoData():
    FIECD = Baseloader("FetterInfoExcelConfigData.json")
    final = {}
    for item in FIECD:
        loc = FIECD.index(item)
        temp = {}
        temp.update({"Origin" : FIECD[loc]["AvatarNativeTextMapHash"]})
        temp.update({"Element" : FIECD[loc]["AvatarVisionBeforTextMapHash"]})
        if FIECD[loc]["AvatarVisionBeforTextMapHash"] == 967031460:
            temp.update({"Constellation" : FIECD[loc]["AvatarConstellationAfterTextMapHash"]})
        else:
            temp.update({"Constellation" : FIECD[loc]["AvatarConstellationBeforTextMapHash"]})

        temp.update({"Title" : FIECD[loc]["AvatarTitleTextMapHash"]})
        temp.update({"Intro" : FIECD[loc]["AvatarDetailTextMapHash"]})
        final.update({FIECD[loc]["AvatarId"] : temp})
    with open(FIDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ProudSkillData():
    PSECD = Baseloader("ProudSkillExcelConfigData.json")
    final = {}
    for item in PSECD:
        loc = PSECD.index(item)

        if PSECD[loc]["ProudSkillGroupId"] not in final:
            temp = item
            final.update({PSECD[loc]["ProudSkillGroupId"] : [temp]})

        else:
            temp = {}
            temp = item
            final[PSECD[loc]["ProudSkillGroupId"]].append(temp)
    with open(PSDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def AvatarPromoteData():
    APECD = Baseloader("AvatarPromoteExcelConfigData.json")
    final = {}
    for item in APECD:
        loc = APECD.index(item)
        if APECD[loc]["AvatarPromoteId"] not in final:
            temp = item
            final.update({APECD[loc]["AvatarPromoteId"] : [temp]})

        else:
            temp = {}
            temp.update({item["PromoteLevel"] : {}})
            temp2 = ["","","",""]
            for bon in item["AddProps"]:
                if bon["PropType"]== "FIGHT_PROP_BASE_HP":
                    temp2[0] = bon
                if bon["PropType"]== "FIGHT_PROP_BASE_ATTACK":
                    temp2[1] = bon
                if bon["PropType"]== "FIGHT_PROP_BASE_DEFENSE":
                    temp2[2] = bon
                else:
                    temp2[3] = bon
            temp3 = []
            for thing in item["CostItems"]:
                try:
                    Idd = str(thing["Id"])
                    #T2 = []
                    temp3.append({
                        "Id" : ItemEN[Idd],
                        "Count" : thing["Count"]
                    })
                except:
                    pass
            item["CostItems"] = temp3
            item["AddProps"] = temp2
            temp = item
            final[APECD[loc]["AvatarPromoteId"]].append(temp)

    with open(APDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def AvatarCurveData():
    ACECD = Baseloader("AvatarCurveExcelConfigData.json")
    final = {}
    temp = [[],[],[],[]]
    for item in ACECD:
        for itemnum in ranger(0,4):
            temp[itemnum].append(item["CurveInfos"][itemnum]["Value"])
    for itemnum in ranger(0,4):
        final.update({ACECD[0]["CurveInfos"][itemnum]["Type"] : temp[itemnum]})


    with open(ACDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ReliquaryData():
    RECD = Baseloader("ReliquaryExcelConfigData.json")
    final = {}
    for item in RECD:
        loc = RECD.index(item)
        final.update({RECD[loc]["Id"] : item})
    with open(RDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def MaterialData():
    MECD = Baseloader("MaterialExcelConfigData.json")
    final = {}
    for item in MECD:
        loc = MECD.index(item)
        final.update({MECD[loc]["Id"] : item})
    with open(MDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def EquipAffixData():
    EAECD = Baseloader("EquipAffixExcelConfigData.json")
    final = {}
    for item in EAECD:
        loc = EAECD.index(item)
        final.update({EAECD[loc]["AffixId"] : item})
    with open(EADP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ReliquarySetData():
    RSECD = Baseloader("ReliquarySetExcelConfigData.json")
    final = {}
    for item in RSECD:
        loc = item
        try:
            final.update({item["SetId"] : item})
        except:
            final.update({0 : item})
    with open(RSDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ManualTextMap():
    MTMCD = Baseloader("ManualTextMapConfigData.json")
    final = {}
    for item in MTMCD:
        temp = {item["TextMapId"] : item}
        final.update(temp)
    with open(MTMP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)


def WeaponCurveData():
    final = {}
    WCECD = Baseloader("WeaponCurveExcelConfigData.json")
    for item in WCECD:
        types = {}
        for dat in item["CurveInfos"]:
            types.update({dat["Type"] : dat["Value"]})        
        
        final.update({item["Level"] : types})
    with open(WCD, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def InvestigationMonsterData():
    final = {}
    WCECD = Baseloader("InvestigationMonsterConfigData.json")
    for item in WCECD:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(IMDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def MaterialSourceDataData():
    final = {}
    WCECD = Baseloader("MaterialSourceDataExcelConfigData.json")
    for item in WCECD:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(MSDDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def MaterialSourceDataData():
    final = {}
    MSDECD = Baseloader("MaterialSourceDataExcelConfigData.json")
    for item in MSDECD:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(MSDDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)
#MSDDP = os.path.join(Remapped_Data, "RewardPreviewExcelConfigData.json")

def RewardPreviewData():
    final = {}
    RPECD = Baseloader("RewardPreviewExcelConfigData.json")
    for item in RPECD:
        if item["Desc"] == "空掉落":
            continue
        temp = {item["Id"] : item}
        final.update(temp)
    with open(RPDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def DisplayItemData():
    final = {}
    DIECD = Baseloader("DisplayItemExcelConfigData.json")
    for item in DIECD:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(DIDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def MechanicBuildingData():
    final = {}
    MBECD = Baseloader("MechanicBuildingExcelConfigData.json")
    for item in MBECD:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(MBDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def GadgetData():
    final = {}
    GECD = Baseloader("GadgetExcelConfigData.json")
    for item in GECD:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(GDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def MechanicusGearLevelUpData():
    final = {}
    MGLUECDP = Baseloader("MechanicusGearLevelUpExcelConfigData.json")
    for item in MGLUECDP:
        if item["GearID"] not in final:
            temp = {item["GearID"] : {item["GearLevel"] : item}}
            final.update(temp)
        else:
            temp = {item["GearLevel"] : item}
            final[item["GearID"]].update(temp)
    with open(MGLUDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def QuestData():
    final = {}
    SP = Baseloader("QuestExcelConfigData.json")
    for item in SP:
        temp = {item["SubId"] : item}
        final.update(temp)
    with open(QDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def MainQuestData():
    final = {}
    SP = Baseloader("MainQuestExcelConfigData.json")
    for item in SP:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(MQDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def TalkData():
    final = {}
    SP = Baseloader("TalkExcelConfigData.json")
    for item in SP:
        if item == {}:
            continue
        temp = {item["Id"] : item}
        final.update(temp)
    with open(TDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def DialogueData():
    final = {}
    SP = Baseloader("DialogExcelConfigData.json")
    for item in SP:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(DDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def NPCData():
    final = {}
    SP = Baseloader("NpcExcelConfigData.json")
    for item in SP:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(NDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def QuestCodexData():
    final = {}
    SP = Baseloader("QuestCodexExcelConfigData.json")
    for item in SP:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(QCDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ChapterData():
    final = {}
    SP = Baseloader("ChapterExcelConfigData.json")
    for item in SP:
        temp = {item["Id"] : item}
        final.update(temp)
    with open(QChDP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)



def Main():
    WeaponPromoteData()
    WeaponData()
    AvatarData()
    AvatarSkillData()
    AvatarSkillDepotData()
    AvatarTalentData()
    FetterStoryData()
    FetterInfoData()
    ProudSkillData()
    AvatarPromoteData()
    AvatarCurveData()
    ReliquaryData()
    EquipAffixData()
    ReliquarySetData()
    ManualTextMap()
    WeaponPromoteData()
    WeaponCurveData()
    MaterialData()
    ForgeData()
    CombineData()
    CookBonusData()
    CookRecipeData()
    InvestigationMonsterData()
    MaterialSourceDataData()
    RewardPreviewData()
    DisplayItemData()
    GadgetData()
    MechanicBuildingData()
    MechanicusGearLevelUpData()
    QuestData()
    MainQuestData()
    TalkData()
    DialogueData()
    NPCData()
    QuestCodexData()
    ChapterData()
ReliquarySetData()