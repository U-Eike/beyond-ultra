import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
langpath = os.path.join(Remapped_Data, "TextEN.json")
Itemdex = os.path.join(Out_Files, "ItemEN.json")
MechMap = os.path.join(Bot_Files, "MechMap.json")

langpath = os.path.join(Remapped_Data, "TextEN.json")

with open(langpath, encoding='utf-8') as temp:
    lang = json.load(temp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

MBD = RMaploader("MechanicBuildingData.json")
MGLUD = RMaploader("MechanicusGearLevelUpData.json")
GD = RMaploader("GadgetData.json")

def tra(Tid):
    return(lang[str(Tid)])
    
def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret
    
def extractor():
    final = {}
    for item in MBD:
        Tower = MBD[item]
        temp = {}
        rewards = []
        for level in MGLUD[item]:
            Tier = MGLUD[item][level]
            lev = {}
            lev.update({"Long" : tra(Tier["DILJPPJOLNA"])})
            lev.update({"Short" : tra(Tier["ILDPCOKMJEF"])})
            if level == "1":
                lev.update({"Upgrade Cost" : 0})
            else:
                lev.update({"Upgrade Cost" : Tier["GearLevelUpMoney"]})
            try:
                lev.update({"Attack" : Tier["Attack"]})
            except:
                lev.update({"Attack" : 0})

            try:
                lev.update({"Speed" : Tier["AttackSpeed"]})
            except:
                lev.update({"Speed" : 0})

            try:
                lev.update({"Range" : Tier["AttackRange"]})
            except:
                lev.update({"Range" : 0})
            lev.update({"Ability Desc" : tra(Tier["DescTextMapHash"])})
            lev.update({"Build Cost" : Tier["BuildCost"]})
            lev.update({"Refund" : Tier["DemolitionRefund"]})

            temp.update({level :lev})


        try:
            temp.update({"Bonus 1" :[MBD[item]["SpecialEffectLevel1"],tra(MBD[item]["FBGJOEIEDCL"])]})
            temp.update({"Bonus 2" :[MBD[item]["SpecialEffectLevel2"],tra(MBD[item]["DHDGFOLOEIA"])]})
        except:
            temp.update({"Bonus 1" :[0,tra(MBD[item]["FBGJOEIEDCL"])]})
            temp.update({"Bonus 2" :[0,tra(MBD[item]["DHDGFOLOEIA"])]})

#        temp.update({"Name" :GD[str(MBD["GadgetId"])]})
#        final.update({tra(MGLUD[item]["1"]["DILJPPJOLNA"]) : temp})
        final.update({tra(MGLUD[item]["1"]["DILJPPJOLNA"]) : temp})

    with open(MechMap, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

extractor()