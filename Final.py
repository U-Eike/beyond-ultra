import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
DB = os.path.join(mydir, "Final")
EndPath = os.path.join(Bot_Files, "Tags.json")



ret = { "Goblet" : "Go",
        "Feather" : "Fe",
        "Headpiece" : "He",
        "Flower" : "Fl",
        "Timepiece" : "Ti"}

demih = { "EQUIP_RING" : "Goblet",
        "EQUIP_NECKLACE" : "Feather",
        "EQUIP_DRESS" : "Headpiece",
        "EQUIP_BRACER" : "Flower",
        "EQUIP_SHOES" : "Timepiece"}

rar = {"QUALITY_BLUE" : 3,"QUALITY_PURPLE" : 4,"QUALITY_ORANGE" : 5}

cwea = { "WEAPON_SWORD_ONE_HAND" : "Sword",
        "WEAPON_CATALYST" : "Catalyst",
        "WEAPON_BOW" : "Bow",
        "WEAPON_CLAYMORE" : "Claymore",
        "WEAPON_POLE" : "Polearm"}


def Botloader(Loc):
    Loc = os.path.join(Bot_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

QChats = Outloader("RawDiag.json")
Bantoggle = True
CharMap = Botloader("CharMap.json")
ArtifMap = Botloader("ArtifMap.json")
WeapMap = Botloader("WeapMap.json")
ItemMap = Botloader("ItemMap.json")
QuestMap = Botloader("QuestMap.json")
TagMap = Botloader("Tags.json")
BS = Botloader("BulkStory.json")
ChapterMap = Botloader("QuestCodexTrees.json")

ArtifEN = os.path.join(DB, "Artif_EN.json")
CharEN = os.path.join(DB, "Char_EN.json")
WeapEN = os.path.join(DB, "Weap_EN.json")
ItemEN = os.path.join(DB, "Item_EN.json")
QuestEN = os.path.join(DB, "Quest_EN.json")
ChapEN =  os.path.join(DB, "Chap_EN.json")
ChapTEN =  os.path.join(DB, "ChapT_EN.json")
SearchEN = os.path.join(DB, "Search_EN.json")

ArtifES = os.path.join(DB, "Artif_ES.json")
CharES = os.path.join(DB, "Char_ES.json")
WeapES = os.path.join(DB, "Weap_ES.json")
ItemES = os.path.join(DB, "Item_ES.json")
QuestES = os.path.join(DB, "Quest_ES.json")
ChapES =  os.path.join(DB, "Chap_ES.json")
SearchES = os.path.join(DB, "Search_ES.json")

ArtifCHS = os.path.join(DB, "Artif_CHS.json")
CharCHS = os.path.join(DB, "Char_CHS.json")
WeapCHS = os.path.join(DB, "Weap_CHS.json")
ItemCHS = os.path.join(DB, "Item_CHS.json")
QuestCHS = os.path.join(DB, "Quest_CHS.json")
ChapCHS =  os.path.join(DB, "Chap_CHS.json")
SearchCHS = os.path.join(DB, "Search_CHS.json")

LangEN = RMaploader("TextEN.json")
LangES = RMaploader("TextES.json")
LangCHS = RMaploader("TextCHS.json")
FIMP = Botloader("RecipeMapData.json")


def conv(st,lang=LangEN):
    st = str(st)
    if st in [""]:
        return ""
    elif st in ["QUALITY_BLUE"]:
        return "3"
    elif st in ["QUALITY_PURPLE"]:
        return "4"
    elif st in ["QUALITY_ORANGE"]:
        return "5"
    elif st in lang.keys():
        return repla(lang[st])
    else:
        print(st)
        return("")

def freog(rec):
    if len(rec) == 1:
        nam = rec[0]["Name"]
    elif len(rec) == 3:
        nrec = [[],[],[]]
        for var in rec:
            if var["Qua"] == "FOOD_QUALITY_ORDINARY":
                nrec[0] = var
            elif var["Qua"] == "FOOD_QUALITY_STRANGE":
                nrec[1] = var
            elif var["Qua"] == "FOOD_QUALITY_DELICIOUS":
                nrec[2] = var
            rec = nrec
        nam = rec[0]["Name"]
    elif len(rec) == 4:
        nrec = [[],[],[],[]]
        for var in rec:
            if var["Qua"] == "FOOD_QUALITY_ORDINARY":
                nrec[0] = var
            elif var["Qua"] == "FOOD_QUALITY_STRANGE":
                nrec[1] = var
            elif var["Qua"]== "FOOD_QUALITY_DELICIOUS":
                nrec[2] = var
            elif var["Qua"] == "Specialty":
                nrec[3] = var
            rec = nrec
        nam = rec[0]["Name"]
    else:
        nam = ""
    return [rec,nam]

def repla(st):
    st = st.replace("<color=#FF9999FF>","**")##Pyro Replace
    st = st.replace("<color=#FFE14BFF>","**")##Null Replace
    st = st.replace("<color=#80FFD7FF>","**")##Anemo Replace
    st = st.replace("<color=#FFD780FF>","**")##Anemo Replace
    st = st.replace("<color=#99FFFFFF>","**")##Cryo Replace
    st = st.replace("<color=#80C0FFFF>","**")##Hydro Replace
    st = st.replace("<color=#FFACFFFF>","**")##Electro Replace
    st = st.replace("<color=#FFE699FF>","**")##Geo Replace
    st = st.replace("<i>","_")##Italic Replace
    st = st.replace("</i>","_")##Italic Replace
    st = st.replace("<color=#FFD780FF>","**")##Unknown Replace
    st = st.replace("\u00b7","\t-")##Bullet Point Replace
    st = st.replace("\\n","\n")##Double Tab Replace
    st = st.replace("\\\\n","\\n")##Double Tab Replace
    st = st.replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}","Tap")##Hold Prompt Replace
    st = st.replace("#","")##Italic Replace
    st = st.replace("</color>","**")##Color End Replace
    return st

def TagInitializer():
    final = {}
    final.update({"Characters" : {}})
    final.update({"Artifacts" : {}})
    final.update({"Materials" : {}})
    final.update({"Weapons" : {}})
    for item in CharMap:
        Char = CharMap[item]
        temp = {}
        temp.update({"Ban" : False})
        temp.update({"Tags" : []})
        temp.update({"Helper" : Char["Helper"]})
        temp.update({"Name" : LangEN[str(CharMap[item]["Name"])]})
        final["Characters"].update({item : temp})

    for item in ArtifMap:
        ASet = ArtifMap[item]
        temp = {}
        temp.update({"Ban" : False})
        temp.update({"Tags" : []})
        temp.update({"Helper" : LangEN[ASet["Set Name"]]})
        final["Artifacts"].update({item : temp})

    for item in WeapMap:
        Weap = WeapMap[item]
        temp = {}
        temp.update({"Ban" : False})
        temp.update({"Tags" : []})
        temp.update({"Helper" : LangEN[item]})
        final["Weapons"].update({item : temp})


    for Tree in ItemMap:
        if Tree in ["Food","Ascension Materials"]:
            for item in ItemMap[Tree]:
                for tier in ItemMap[Tree][item]:
                    Mat = ItemMap[Tree][item][tier]
                    temp = {}
                    temp.update({"Ban" : False})
                    temp.update({"Tags" : []})
                    temp.update({"Helper" : Mat["Helper"]})
                    temp.update({"Type" : Tree})
                    final["Materials"].update({tier : temp})    
        else:
            for item in ItemMap[Tree]:
                Mat = ItemMap[Tree][item]
                temp = {}
                temp.update({"Ban" : False})
                temp.update({"Tags" : []})
                temp.update({"Helper" : Mat["Helper"]})
                temp.update({"Type" : Tree})
                final["Materials"].update({item : temp})    


    with open(EndPath, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def TagRevitializer():
    final = Botloader("Tags.json")
    for item in CharMap:

        if item in final["Characters"]:
            Char = CharMap[item]
            temp = {}
            temp.update({"Ban" : final["Characters"][item]["Ban"]})
            temp.update({"Tags" : final["Characters"][item]["Tags"]})
            temp.update({"Helper" : Char["Helper"]})
            temp.update({"Name" : conv(str(Char["Name"]))})
            temp.update({"Element" : conv(str(Char["Element"]))})
            temp.update({"Rarity" : conv(str(Char["Rarity"]))})
            final["Characters"].update({item : temp})
            #continue
        else:
            Char = CharMap[item]
            temp = {}
            temp.update({"Ban" : True})
            temp.update({"Tags" : []})
            temp.update({"Helper" : Char["Helper"]})
            temp.update({"Name" : conv(str(Char["Name"]))})
            temp.update({"Element" : conv(str(Char["Element"]))})
            temp.update({"Rarity" : conv(str(Char["Rarity"]))})
            final["Characters"].update({item : temp})

    for item in ArtifMap:
        if item in final["Artifacts"]:
            continue
        else:
            ASet = ArtifMap[item]
            temp = {}
            temp.update({"Ban" : True})
            temp.update({"Tags" : []})
            temp.update({"Helper" : LangEN[ASet["Set Name"]]})
            final["Artifacts"].update({item : temp})

    for item in WeapMap:
        if item in final["Weapons"]:
            Weap = WeapMap[item]
            temp = {}
            temp.update({"Ban" : final["Weapons"][item]["Ban"]})
            temp.update({"Tags" : final["Weapons"][item]["Tags"]})
            temp.update({"Helper" : Weap["Series"]})
            temp.update({"Name" : LangEN[Weap["Name"]]})
            temp.update({"Type" : Weap["Class"]})
            final["Weapons"].update({item : temp})
        else:
            Weap = WeapMap[item]
            temp = {}
            temp.update({"Ban" : True})
            temp.update({"Tags" : []})
            temp.update({"Helper" : Weap["Series"]})
            temp.update({"Name" : LangEN[Weap["Name"]]})
            temp.update({"Type" : Weap["Class"]})
            final["Weapons"].update({item : temp})

    for item in QuestMap:
        if item in final["Quests"]:
            Quest = QuestMap[item]
            temp = {}
            temp.update({"DB Dia" : final["Quests"][item]["DB Dia"]})
            temp.update({"Ban" : final["Quests"][item]["Ban"]})
            temp.update({"Name" : conv(Quest["Title"])})
            temp.update({"Desc" : conv(Quest["Desc"])})
            final["Quests"].update({item : temp})
        else:
            Quest = QuestMap[item]
            temp = {}
            temp.update({"DB Dia" : False})
            temp.update({"Ban" : True})
            temp.update({"Name" : conv(Quest["Title"])})
            temp.update({"Desc" : conv(Quest["Desc"])})
            final["Quests"].update({item : temp})


    for Tree in ItemMap:
        if Tree in ["Ascension Materials"]:
            for item in ItemMap[Tree]:
                for tier in ItemMap[Tree][item]:
                    if tier in final["Items"]:
                        Mat = ItemMap[Tree][item][tier]
                        final["Items"][tier]["Helper"] = Mat["Helper"]
                    else:

                        Mat = ItemMap[Tree][item][tier]
                        temp = {}
                        temp.update({"Ban" : True})
                        temp.update({"Tags" : []})
                        temp.update({"Helper" : Mat["Helper"]})
                        temp.update({"Type" : Tree})
                        final["Items"].update({tier : temp})    
        else:
            for item in ItemMap[Tree]:
                if item in final["Items"]:
                    Mat = ItemMap[Tree][item]
                    final["Items"][item]["Helper"] = Mat["Helper"]
                    continue
                else:
                    Mat = ItemMap[Tree][item]
                    temp = {}
                    temp.update({"Ban" : True})
                    temp.update({"Tags" : []})
                    temp.update({"Helper" : Mat["Helper"]})
                    temp.update({"Type" : Tree})
                    final["Items"].update({item : temp})    


    with open(EndPath, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ArtifMapper(lan,Out):
    fin = {}
    for item in ArtifMap:
        temp = {}
        bons = []
        for bon in ArtifMap[item]["Set Bonuses"]:
            bons.append([bon,conv(ArtifMap[item]["Set Bonuses"][bon][1],lan)])
        SetPie = {}
        for Pie in ArtifMap[item]["Set Pieces"]:
            temp2 = {}
            #nam = demih[Pie]
            #sto = Tag[item][nam]
            if str(item) not in BS["Artifacts"]:
                temp2.update({Pie : [conv(ArtifMap[item]["Set Pieces"][Pie][0],lan),conv(ArtifMap[item]["Set Pieces"][Pie][1],lan),"",str(item)+"_"+ret[Pie]]})
            else:        
                temp2.update({Pie : [conv(ArtifMap[item]["Set Pieces"][Pie][0],lan),conv(ArtifMap[item]["Set Pieces"][Pie][1],lan),BS["Artifacts"][str(item)][Pie],str(item)+"_"+ret[Pie]]})

            SetPie.update(temp2)
        temp.update({"Bonuses" : bons})
        temp.update({"SID" : item})
        #temp.update({"Tags" : Tag[item]["Tags"]})
        temp.update({"Set Pieces" : SetPie})
        fin.update({conv(ArtifMap[item]["Set Name"],lan) :  temp  })
    sort = sorted(fin.keys())
    final = {}
    for item in sort:
        final.update({item : fin[item]})
    with open(Out, "w") as write_file:
        json.dump(fin, write_file, indent=4)

def CharMapper(lan,out):
    fin = {}
    for item in CharMap:
        if TagMap["Characters"][item]["Ban"] and Bantoggle:
            continue
        temp = {
                "Name": conv(CharMap[item]["Name"],lan),
                "Title": conv(CharMap[item]["Title"],lan),
                "Element": conv(CharMap[item]["Element"],lan),
                "Affiliation": conv(CharMap[item]["Origin"],lan),
                "Rarity": rar[CharMap[item]["Rarity"]],
                "Class": cwea[CharMap[item]["Weapon"]],
                "Constellation": conv(CharMap[item]["Constellation"],lan),
                "Intro": conv(CharMap[item]["Intro"],lan),

                "Normal Atk": [conv(CharMap[item]["Normal Attack"]["Name"],lan),conv(CharMap[item]["Normal Attack"]["Description"],lan),CharMap[item]["Normal Attack"]["Table Data"],CharMap[item]["Normal Attack"]["Total"],CharMap[item]["Normal Attack"]["Upgrade"]],

                "Elemental Skill": [conv(CharMap[item]["Elemental Skill"]["Name"],lan),conv(CharMap[item]["Elemental Skill"]["Description"],lan),CharMap[item]["Elemental Skill"]["Table Data"],CharMap[item]["Elemental Skill"]["Total"],CharMap[item]["Elemental Skill"]["Upgrade"]],

                "Elemental Burst": [conv(CharMap[item]["Elemental Burst"]["Name"],lan),conv(CharMap[item]["Elemental Burst"]["Description"],lan),CharMap[item]["Elemental Burst"]["Table Data"],CharMap[item]["Elemental Burst"]["Total"],CharMap[item]["Elemental Burst"]["Upgrade"]]
            }
        Constellations = {}
        for talnum in [1,2,3,4,5,6]:
            ntalnum = str(talnum)
            Constellations.update({ntalnum :[conv(CharMap[item]["Talents"][ntalnum]["Name"],lan),conv(CharMap[item]["Talents"][ntalnum]["Description"],lan)]})
        temp.update({"Talents" : Constellations})
        History = {}
        for stonum in [0,1,2,3,4,5]:
            nstonum = str(stonum)
            History.update({nstonum :[conv(CharMap[item]["Story"][nstonum]["Name"],lan),conv(CharMap[item]["Story"][nstonum]["Description"],lan)]})
        History.update({"Fave" :[conv(CharMap[item]["Story"]["Fave"]["Name"],lan),conv(CharMap[item]["Story"]["Fave"]["Description"],lan)]})
        History.update({"Focus" :[conv(CharMap[item]["Story"]["Focus"]["Name"],lan),conv(CharMap[item]["Story"]["Focus"]["Description"],lan)]})
        Passives = []
        for TempPas in CharMap[item]["Passives"]:
            if TempPas != ["",""]:
                Passives.append([conv(TempPas[0],lan),conv(TempPas[1],lan)])
            else:
                Passives.append(TempPas)
        Tg = TagMap["Characters"][item]["Tags"]
        Tg.append(str(rar[CharMap[item]["Rarity"]]))
        Tg.append(str(cwea[CharMap[item]["Weapon"]]))
        Tg.append(conv(CharMap[item]["Element"],lan))
        temp.update({"Tags" : Tg})


        temp.update({"Passives" : Passives})

        temp.update({"Story" : History})
        temp.update({"Stats" : CharMap[item]["Stats"]})
        temp.update({"Ascension" : CharMap[item]["Ascension"]})
        temp.update({"Total" : CharMap[item]["total"]})
    
        fin.update({conv(CharMap[item]["Name"],lan) : temp})

    fin2 = {}
    for item in sorted(fin.keys()):
        fin2.update({item : fin[item]})

    with open(out, "w") as write_file:
        json.dump(fin2, write_file, indent=4)

def WeapMapper(lan,out):
    fin = {}
    for item in WeapMap:
        if TagMap["Weapons"][item]["Ban"]:
            continue
        temp = {}
        temp.update({"Rarity" : WeapMap[item]["Rarity"]})
        temp.update({"Description" : conv(WeapMap[item]["Description"],lan)})
        if WeapMap[item]["Class"] == "Pole":
            temp.update({"Class" : "Polearm"})
        else:
            temp.update({"Class" : WeapMap[item]["Class"]})
        temp.update({"Series" : WeapMap[item]["Series"]})
        try:
            temp.update({"Story" : repla(BS["Weapons"][str(item)])})
        except:
            print(item+" Missing ")
        subtemp = {}        
        temp.update({"Substat Type" : conv(WeapMap[item]["Substat Type"],lan)})
        for det in WeapMap[item]["Passive"]:
            subtemp.update({det :   conv(WeapMap[item]["Passive"][det],lan) })
        temp.update({"Passive" : subtemp})
        temp.update({"Stats" : WeapMap[item]["Stats"]})
        temp.update({"Tags" : []})
        for tagitem in TagMap["Weapons"][item]["Tags"]:
            if tagitem in lan.keys():
                temp["Tags"].append(conv(tagitem,lan))
            else:
                temp["Tags"].append(tagitem)
        temp["Tags"].append(str(temp["Rarity"]))
        temp["Tags"].append(temp["Class"])
        temp["Tags"].append(temp["Substat Type"])
        temp.update({"Ascension" : WeapMap[item]["Ascension"]})
        temp.update({"Total" : WeapMap[item]["total"]})
        #print(temp["Passive"]["1"])
        ref = []
        ref2 = []
        for PNum in ["1","2","3","4","5"]:
            ref.append(re.findall("\*\*(.*?)\*\*", temp["Passive"][PNum]))
        for div in ref[0]:
            ref2.append([])
        for div in ref:
            for div2 in div:
                ref2[div.index(div2)].append("**"+div2+"**")
        Nref = temp["Passive"]["1"]
        for div in ref2:
            index = ref2.index(div)
            rep = "/".join(div)
            if "%" in rep:
                rep  = rep.replace("%","")+"%"
            Nref = Nref.replace("**"+ref[0][index]+"**",rep)
        temp["Passive"].update({"Refinement" : Nref})

        fin.update({conv(WeapMap[item]["Name"],lan) : temp})
    sort = (fin.keys())
    final = {}
    for item in sort:
        final.update({item : fin[item]})
    with open(out, "w") as write_file:
        json.dump(final, write_file, indent=4)

FTyp = {"UI_Buff_Item_Recovery_HpAdd" : "Burst Restorative",
        "UI_Buff_Item_Recovery_HpAddAll" : "Regenerative",
        "UI_Buff_Item_Recovery_Revive" : "Revivicative ",
        "UI_Buff_Item_Other_SPAdd" : "Stamina Restore",
        "UI_Buff_Item_Other_SPReduceConsume" : "Stamina Cost Down ",
        "UI_Buff_Item_Atk_Add" : "Base Atk Boost",
        "UI_Buff_Item_Atk_CritRate" : "Crit Rate Boost",
        "UI_Buff_Item_Climate_Heat" : "Warming",
        "UI_Buff_Item_Def_Resistance_Fire" : "Pyro Resist",
        "UI_Buff_Item_Def_Resistance_Water" : "Hydro Resist",
        "UI_Buff_Item_Def_Resistance_Wind" : "Anemo Resist",
        "UI_Buff_Item_Def_Resistance_Rock" : "Geo Resist",
        "UI_Buff_Item_Def_Resistance_Ice" : "Cryo Resist",
        "UI_Buff_Item_Def_Resistance_Elect" : "Electro Resist",
        "UI_Buff_Item_Atk_ElementHurt_Fire" : "Pyro Damage",
        "UI_Buff_Item_Atk_ElementHurt_Water" : "Hydro Damage",
        "UI_Buff_Item_Atk_ElementHurt_Wind" : "Anemo Damage",
        "UI_Buff_Item_Atk_ElementHurt_Rock" : "Geo Damage",
        "UI_Buff_Item_Atk_ElementHurt_Ice" : "Cryo Damage",
        "UI_Buff_Item_Atk_ElementHurt_Elect" : "Electro Damage",
        "UI_Buff_Item_Climate_Heat" : "Warming",
        "UI_Buff_Item_Def_Add" : "Defensive"}

def ItemMapper(lan,out):
    fin = {}
    fin.update({"Food" : {}})
    fin.update({"Potion" : {}})
    fin.update({"Ascension" : {}})
    fin.update({"Mats" : {}})
    nmap = ItemMap["Food"]
    checked = ["108000","108147"]
    for item in FIMP:
        fintemp = {}
        Variants = []
        for food in FIMP[item]["Variants"]:
            temp = {}

            if food not in nmap:
                print(str(food)+" Food Missing")
                food = "108005"
            FIDS = nmap[food]
            temp.update({"Name" : conv(FIDS["Item Name"],lan)})
            temp.update({"Description" : conv(FIDS["Item Desc"],lan)})
            temp.update({"Effect" : conv(FIDS["Effect Desc"],lan)})
            checked.append(food)
            Variants.append(temp)
        fintemp.update({"Variants" : Variants})
        fintemp.update({"Recipe" : FIMP[item]["Materials"]})
        fintemp.update({"Unique" : True})
        fintemp.update({"Category" : conv(FIMP[item]["Type"],lan)})
        try:
            fintemp.update({"Type" : FTyp[nmap[FIMP[item]["Variants"][0]]["IC"]]})
        except:
            fintemp.update({"Type" : "Unkw"})
        if FIMP[item]["Specialty"][0]:
            fintemp.update({"Specialty" : [True,conv(CharMap[FIMP[item]["Specialty"][1]]["Name"],lan)]})
        else:
            fintemp.update({"Specialty" : [False]})


        fin["Food"].update({conv(FIMP[item]["Name"],lan) :  fintemp  })



    for item in nmap:
        if TagMap["Items"][item]["Ban"] or item in checked:
            continue
        temp = {}
        temp.update({"Name" : conv(nmap[item]["Item Name"],lan)})
        temp.update({"Description" : conv(nmap[item]["Item Desc"],lan)})
        temp.update({"Effect" : conv(nmap[item]["Effect Desc"],lan)})
        Sou = []
        for SRC in nmap[item]["Sources"]:
            if conv(SRC,lan) in [""]:
               continue
            Sou.append(conv(SRC,lan))
        temp.update({"Sources" : Sou})
        temp.update({"Unique" : False})
        temp.update({"Type" : FTyp[nmap[item]["IC"]]})
        nam = conv(nmap[item]["Item Name"],lan)

        if item == "701":
            fin["Potion"].update({item :  temp  })
        else:
            fin["Food"].update({nam :  temp  })
    

    nmap = ItemMap["Ascension Materials"]
    ASCM = ["Brilliant Diamond","Bone Shard","Aerosiderite","Mist Veiled","from Guyun","Dandelion Gladiator","Boreal Wolf","Decarabian","Sacrificial Knife","Mist Grass","Chaos","Ley Line","Statuette","Horn","Arrowhead","Scroll","Mask","Slime","Gold","Diligence","Prosperity","Ballad","Nectar","Resistance","Freedom","Prithiva Topaz","Shivada Jade","Vayuda Turquoise","Vajrada Amethyst","Nagadus Emerald","Varunada Lazurite","Agnidus Agate"]
    for item in nmap:
        for tier in nmap[item]:
            if TagMap["Items"][tier]["Ban"]:
                continue
            temp = {}
            for aset in ASCM:
                if item == "111":
                    loc = "Fatui Insignia"
                elif item == "112":
                    loc = "Hoarder Insignia"
                elif aset.lower() in conv(nmap[item][tier]["Item Name"],lan).lower():
                    loc = aset
                    break
                else:
                    loc = tier
            temp.update({"Description" : conv(nmap[item][tier]["Item Desc"],lan)})
            temp.update({"Group" : loc})
            temp.update({"Characters" : []})
            temp.update({"Weapons" : []})
            temp.update({"Food Recipes" : []})
            temp.update({"CharTalents" : []})
            Sou = []
            for SRC in nmap[item][tier]["Sources"]:
                if conv(SRC,lan) in [""]:
                    continue
                Sou.append(conv(SRC,lan))
            temp.update({"Sources" : Sou})
            fin["Mats"].update({conv(nmap[item][tier]["Item Name"],lan) : temp})


    nmap = ItemMap["Materials"]
    for item in nmap:
        if TagMap["Items"][item]["Ban"]:
            continue
        temp = {}
        for aset in ASCM:
            if item == "111":
                loc = "Fatui Insignia"
            elif item == "112":
                loc = "Hoarder Insignia"
            elif aset.lower() in conv(nmap[item]["Item Name"],lan).lower():
                loc = aset
                break
            else:
                loc = tier
        temp.update({"Description" : conv(nmap[item]["Item Desc"],lan)})
        temp.update({"Group" : True})
        temp.update({"Characters" : []})
        temp.update({"Character Skills" : []})
        temp.update({"Food Recipes" : []})
        temp.update({"Weapons" : []})
        temp.update({"CharTalents" : []})
        Sou = []
        for SRC in nmap[item]["Sources"]:
            if conv(SRC,lan) in [""]:
               continue
            Sou.append(conv(SRC,lan))
            temp.update({"Sources" : Sou})
        fin["Mats"].update({conv(nmap[item]["Item Name"],lan) : temp})

    for Weapon in WeapMap:
        for mat in WeapMap[Weapon]["total"]:
            if TagMap["Weapons"][Weapon]["Ban"] == False:
                nam = conv(Weapon,lan)
                fin["Mats"][mat]["Weapons"].append([conv(str(WeapMap[Weapon]["Name"]),lan),WeapMap[Weapon]["total"][mat]])
    for Character in CharMap:
        if TagMap["Characters"][Character]["Ban"]:
            continue
        for mat in CharMap[Character]["total"]:
            if TagMap["Characters"][Character]["Ban"] == False and mat in fin["Mats"]:
                fin["Mats"][mat]["Characters"].append([conv(str(CharMap[Character]["Name"]),lan),CharMap[Character]["total"][mat]])
        for mat in CharMap[Character]["Normal Attack"]["Total"]:
                if TagMap["Characters"][Character]["Ban"] == False and mat in fin["Mats"]:
                    fin["Mats"][mat]["CharTalents"].append([conv(str(CharMap[Character]["Name"]),lan),CharMap[Character]["Normal Attack"]["Total"][mat]])
    """
    for Character in CharMap:
        for mat in CharMap[Character]["Normal Attack"]["Total"]:
            if TagMap["Characters"][Character]["Ban"] == False and mat in fin["Mats"]:
                fin["Mats"][mat]["CharTalents"].append([conv(str(CharMap[Character]["Name"]),lan),CharMap[Character]["total"][mat]])
    """
    for Food in FIMP:

        for mat in FIMP[Food]["Materials"]:
            if FIMP[Food]["Variants"][0] not in TagMap["Items"]:
                fin["Mats"][mat[0]]["Food Recipes"].append([conv(str(FIMP[Food]["Name"]),lan),mat[1]])
            elif TagMap["Items"][FIMP[Food]["Variants"][0]]["Ban"] == False and mat[0] in fin["Mats"]:
                fin["Mats"][mat[0]]["Food Recipes"].append([conv(str(FIMP[Food]["Name"]),lan),mat[1]])


    with open(out, "w") as write_file:
        json.dump(fin, write_file, indent=4)

DD = RMaploader("DialogueData.json")
ND = RMaploader("NPCData.json")
BQD = RMaploader("QuestData.json")
QCD = RMaploader("QuestChapterData.json")

def delver(Hole,lan):
    End = []
    #if type(Hole) == int:
        #Hole = str(Hole)
    for item in Hole:
        if type(item) == list:
            for Branch in item:
                End.append(delver(Branch,lan))
            
        else:
            DDL = DD[str(item)]
            NPCN = DDL["TalkRole"]
            if NPCN["Id"] in ["","主角","玩家","0","****"]:
                if "Type" in NPCN:
                    if NPCN["Type"] == "TALK_ROLE_MATE_AVATAR":
                        Name = "Sibling"
                    else:
                        Name = "Traveler"
                else:
#            if NPCN["Type"] in ["TALK_ROLE_PLAYER"]:
                    Name = "Traveler"
            else:
                Name = conv(ND[NPCN["Id"]]["NameTextMapHash"],lan)
            End.append(Name+" : "+conv(DDL["TalkContentTextMapHash"],lan))
    return(End)

def QuestMapper(lan,Out):
    fin = {}
    for MQuest in QuestMap:
        CQ = QuestMap[MQuest]
        temp = {}
        temp.update({"Name" : conv(CQ["Title"])})
        temp.update({"Desc" : conv(CQ["Desc"])})
        fin.update({MQuest :  temp  })
    if lan in [LangCHS]:
        with open(Out, "w", encoding= 'utf-8') as write_file:
            json.dump(fin, write_file, indent=4, ensure_ascii=False)
    else:
        with open(Out, "w") as write_file:
            json.dump(fin, write_file, indent=4)

def ChapterMapper(lan,Out):
    fin = {}
    Cfin = {}
    for Quest in ChapterMap:
        Nam = conv(QCD[Quest]["ChapterNumTextMapHash"],lan)
        ChapNam = conv(QCD[Quest]["ChapterTitleTextMapHash"],lan)
        Foc = ChapterMap[Quest]
        temp = {}
        for SubQuest in Foc:
            SubFoc = str(SubQuest)
            SubNam = conv(QuestMap[SubFoc]["Title"],lan)
            temp.update({SubNam : {}})

        if Nam == "":
            Nam = QCD[Quest]["ChapterIcon"]

        fin.update({Nam+" - "+ChapNam :  temp  })


    for Quest in QCD:
        Foc = QCD[Quest]
        NNam = conv(str(Foc["ChapterNumTextMapHash"]))
        if NNam == "":
            NNam = Quest
        temp = {}
        if "BeginQuestId" in Foc:
            temp.update({"Begin" : Foc["BeginQuestId"]})
            temp.update({"End" : Foc["EndQuestId"]})
            temp.update({"Contains" : {}})
        else:
            temp.update({"Begin" : 0})
            temp.update({"End" : 0})
            temp.update({"Contains" : {}})
        Cfin.update({NNam : temp})


    for Chapter in Cfin:
        Chap = Cfin[Chapter]
        for item in range(Chap["Begin"],Chap["End"]):
            if str(item) in BQD:
                Cfin[Chapter]["Contains"].update({item : ""})
    if lan in [LangCHS]:
        with open(Out, "w", encoding= 'utf-8') as write_file:
            json.dump(fin, write_file, indent=4, ensure_ascii=False)
    else:
        with open(Out, "w") as write_file:
            json.dump(fin, write_file, indent=4)
        with open(ChapTEN, "w") as write_file:
            json.dump(Cfin, write_file, indent=4)

def SearchMapper(CData,AData,WData,IData,Out):
    with open(CData) as f:
        CData = json.load(f)
    with open(AData) as f:
        AData = json.load(f)
    with open(WData) as f:
        WData = json.load(f)
    with open(IData) as f:
        IData = json.load(f)
        MData = IData["Mats"]
        FData = IData["Food"]

    newdata = {"Weapon" : {},
                "Character" : {},
                "Artifact" : {},
                "Piece" : {},
                "Food" : {},
                "Mats" : {}
    }

    SWKeys = sorted(WData.keys())
    id = 0
    for item in SWKeys:
        newdata["Weapon"][item]= {
                        "NID": id,
                        "Tags" : WData[item]["Tags"]
        }
        id+=1

    SCKeys = sorted(CData.keys())
    id = 0
    for item in SCKeys:
        newdata["Character"][item]= {
                        "NID": id,
                        "Tags" : CData[item]["Tags"]
        }
        id+=1

    SAKeys = sorted(AData.keys())
    id = 0
    idp = 0
    for item in SAKeys:
        newdata["Artifact"][SAKeys[id]]= {
                        "NID": id
                        }
        for pie in AData[item]["Set Pieces"]:
            newdata["Piece"][AData[item]["Set Pieces"][pie][0]] = {"NID" : idp,
                                                                "Set" : item,
                                                                "Part" : pie}
            idp+=1

        id+=1

    SFKeys = sorted(FData.keys())
    id = 0
    for item in SFKeys:
        newdata["Food"][item]= {
                        "NID": id
        }
        id+=1

    SMKeys = list(MData.keys())
    id = 0
    for item in SMKeys:
        newdata["Mats"][item]= {
                        "NID": id
        }
        id+=1

    with open(Out, "w") as write_file:
        json.dump(newdata, write_file,indent =4)


def main():
    TagRevitializer()
    Languages = [LangEN,LangES,LangCHS]
    ArtifOut = [ArtifEN,ArtifES,ArtifCHS]
    CharOut = [CharEN,CharES,CharCHS]
    WeapOut = [WeapEN,WeapES,WeapCHS]
    ItemOut = [ItemEN,ItemES,ItemCHS]
    QuestOut = [QuestEN,QuestES,QuestCHS]

    for LanPan in [0]:
        ArtifMapper(Languages[LanPan],ArtifOut[LanPan])
        CharMapper(Languages[LanPan],CharOut[LanPan])
        WeapMapper(Languages[LanPan],WeapOut[LanPan])
        ItemMapper(Languages[LanPan],ItemOut[LanPan])
        QuestMapper(Languages[LanPan],QuestOut[LanPan])
        SearchMapper(CharEN,ArtifEN,WeapEN,ItemEN,SearchEN)
if __name__ == "__main__":
    #main()
    #QuestMapper(LangEN,QuestEN)
    #ChapterMapper(LangEN,ChapEN)
    TagRevitializer()
#TagInitializer()
#main()
#