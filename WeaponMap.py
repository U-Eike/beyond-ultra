import sys
import os
from pathlib import Path
import json
import math
from openpyxl import Workbook


os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
ENLP = os.path.join(Remapped_Data, "TextEN.json")
EADP = os.path.join(Remapped_Data, "EquipAffixData.json")
MTMP = os.path.join(Remapped_Data, "ManualTextMap.json")
WDP = os.path.join(Remapped_Data, "WeaponData.json")
WPDP = os.path.join(Remapped_Data, "WeaponPromoteData.json")
Curve = os.path.join(Out_Files, "Weapons.json")
Itemu = os.path.join(Out_Files, "ItemEN.json")
Tabula = os.path.join(Out_Files,"WeaponTables.json")
WMP = os.path.join(Bot_Files,"WeapMap.json")
#Weapon Files


with open(ENLP, encoding='utf-8') as temp:
    ENL = json.load(temp)
with open(WDP, encoding='utf-8') as temp:
    WD = json.load(temp)
with open(EADP, encoding='utf-8') as temp:
    EAD = json.load(temp)
with open(MTMP, encoding='utf-8') as temp:
    MTM = json.load(temp)
with open(WPDP, encoding='utf-8') as temp:
    WPD = json.load(temp)
with open(WMP, encoding='utf-8') as temp:
    WM = json.load(temp)
with open(Tabula, encoding='utf-8') as temp:
    Rasa = json.load(temp)
with open(Itemu, encoding='utf-8') as temp:
    ItemEN = json.load(temp)


def extractor():
    final = {}
    NEAD = {}
        
    for wep in WD:
        item = WD[wep]
        temp = {}
        temp.update({"Name" : str(item["NameTextMapHash"])})
        temp.update({"Description" : str(item["DescTextMapHash"])})
        temp.update({"Story" : ""})
        temp.update({"Rarity" : item["RankLevel"]})
        IC = item["Icon"].split("_")
        temp.update({"Class" : IC[2]})
        temp.update({"Series" : IC[3]})
        temp.update({"Icon" : item["Icon"]})
        temp.update({"AIcon" : item["AwakenIcon"]})
        temp.update({"Sid" : item["Id"]})
        affix = str(item["SkillAffix"][0])
        Evol = []
        Total = {}
        TID = item["Id"]
        if str(TID) not in WPD:
            TID = 11101
        for level in WPD[str(TID)]:
            lv = WPD[str(TID)][level]
            if "Evolution" in lv:
                Evol.append([lv["Evolution"]["Mora"],lv["Evolution"]["Mats"]])
        for level in Evol:
            for mat in level[1]:
                if mat["Id"] in Total:
                    Total[mat["Id"]]+=mat["Count"]
                else:
                    Total.update({mat["Id"] : mat["Count"]})
        TKeys = list(Total.keys())
        if len(TKeys) == 7:
            Total = {  TKeys[0] : Total[TKeys[0]],
                        TKeys[3] : Total[TKeys[3]],
                        TKeys[6] : Total[TKeys[6]],
                        TKeys[1] : Total[TKeys[1]],
                        TKeys[4] : Total[TKeys[4]],
                        TKeys[2] : Total[TKeys[2]],
                        TKeys[5] : Total[TKeys[5]]
            }
        else:
            Total = {  TKeys[0] : Total[TKeys[0]],
                        TKeys[3] : Total[TKeys[3]],
                        TKeys[6] : Total[TKeys[6]],
                        TKeys[9] : Total[TKeys[9]],
                        TKeys[1] : Total[TKeys[1]],
                        TKeys[4] : Total[TKeys[4]],
                        TKeys[7] : Total[TKeys[7]],
                        TKeys[2] : Total[TKeys[2]],
                        TKeys[5] : Total[TKeys[5]],
                        TKeys[8] : Total[TKeys[8]]
            }


        temp.update({"Ascension" : Evol})
        temp.update({"total" : Total})
        try:
            passive = { "Name" : str(EAD[affix+"0"]["NameTextMapHash"]),
                        "1" : str(EAD[affix+"0"]["DescTextMapHash"]),
                        "2" : str(EAD[affix+"1"]["DescTextMapHash"]),
                        "3" : str(EAD[affix+"2"]["DescTextMapHash"]),
                        "4" : str(EAD[affix+"3"]["DescTextMapHash"]),
                        "5" : str(EAD[affix+"4"]["DescTextMapHash"])
            }
        except:
            passive = { "Name" : "None",
                        "1" : "N/A",
                        "2" : "N/A",
                        "3" : "N/A",
                        "4" : "N/A",
                        "5" : "N/A"
            }
        if affix == "111412":
            passive["Name"] = "613846163"
            passive["1"] = "2090107097"

        temp.update({"Passive" : passive})
        temp.update({"Substat Type" : Rasa[str(item["Id"])]["Type"]})
        temp.update({"Affix" : affix})

        stats = []
        Tabl = Rasa[str(item["Id"])]["Stats"]
        try:
            for level in [0,1,20,21,41,42,52,53,63,64,74,75,85,86,96]:
                stats.append(Tabl[level])
        except:
            for level in [0,1,20,21,41,42,52,53,63,64,74]:
                stats.append(Tabl[level])
        temp.update({"Stats" : stats})


        final.update({wep : temp})

    with open(WMP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)



def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret



def initmanual():
    final = {}
        
    for item in WM:
        temp = {}
        Wtype = {}
        Stype = {}
        Tags = [WM[item]["Class"],WM[item]["Substat Type"]]
        temp.update({"Tags" : Tags})
        final.update({item : temp})

    with open("WeaponTagsjson", "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

#haktotables()
#haktoout()
def Main():
    extractor()
    
if __name__ == "__main__":
    Main()
#ReMap()
#weapontables()

#linker()

#initmanual()
