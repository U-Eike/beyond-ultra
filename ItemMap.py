import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
MECDP = os.path.join(Base_Data, "MaterialExcelConfigData.json")
MDP = os.path.join(Remapped_Data, "MaterialData.json")
MTMP = os.path.join(Remapped_Data, "ManualTextMap.json")
langpath = os.path.join(Remapped_Data, "TextEN.json")
IMP = os.path.join(Bot_Files,"ItemMap.json")

with open(MDP, encoding='utf-8') as temp:
    MD = json.load(temp)
with open(MTMP, encoding='utf-8') as temp:
    MTM = json.load(temp)
with open(langpath, encoding='utf-8') as temp:
    lang = json.load(temp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

MSD = RMaploader("MaterialSourceDataData.json")

def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret

def extractor():
    final = {}
    food = {}
    quest = {}
    mats = {}
    rest = {}
    pips = {}
    chests = {}
    cards = {}
    fortuna = {}
    asc = {}
    for item in MD:
        temp = {}
        temp.update({"Item Type" : MD[item]["ItemType"]})
        try:
            temp.update({"Material Type" : MD[item]["MaterialType"]})
        except:
            temp.update({"Material Type" : "None"})
        INam = str(MD[item]["NameTextMapHash"])
        temp.update({"Helper" : lang[INam]})
        temp.update({"Item Name" : INam})
        try:
            temp.update({"Rarity" : MD[item]["RankLevel"]})
        except:
            temp.update({"Rarity" : 0})
        temp.update({"Icon" : MD[item]["Icon"]})
        INde = str(MD[item]["DescTextMapHash"]) 
        temp.update({"Helper2" : lang[INde]})
        Sou = []
        for SRC in MSD[item]["TextList"]:
            if SRC in [""]:
                continue
            Sou.append(str(SRC))
        temp.update({"Sources" : Sou})
        temp.update({"Item Desc" : INde})
        if temp["Material Type"] in ["MATERIAL_FOOD","MATERIAL_NOTICE_ADD_HP"]:
            temp.update({"Ban" : False})
            temp.update({"Effect Desc" : str(MD[item]["EffectDescTextMapHash"])})
            temp.update({"Type" : str(MD[item]["TypeDescTextMapHash"])})
            temp.update({"IC" : str(MD[item]["EffectIcon"])})
            try:
                temp.update({"Quality" : str(MD[item]["FoodQuality"])})
            except:
                temp.update({"Quality" : "Specialty"})
            food.update({item : temp})

        elif temp["Material Type"] == "MATERIAL_EXCHANGE":
            mats.update({item : temp})
        elif temp["Material Type"] == "MATERIAL_ADSORBATE":
            pips.update({item : temp})
        elif temp["Material Type"] == "MATERIAL_CHEST":
            chests.update({item : temp})
        elif temp["Material Type"] == "MATERIAL_AVATAR":
            cards.update({item : temp})
        elif temp["Material Type"] == "MATERIAL_QUEST":
            quest.update({item : temp})
        elif temp["Material Type"] == "MATERIAL_TALENT":
            fortuna.update({item : temp})
        elif temp["Material Type"] == "MATERIAL_AVATAR_MATERIAL":
            if MD[item]["Rank"] not in asc:
                asc.update({MD[item]["Rank"] : {item : temp}})
            else:
                asc[MD[item]["Rank"]].update({item : temp})
        else:
            rest.update({item : temp})
    
    final.update({"Materials" : mats})
    final.update({"Food" : food})
    final.update({"Quest" : quest})
    final.update({"Pips" : pips})
    final.update({"Ascension Materials" : asc})
    final.update({"Chests" : chests})
    final.update({"Character Cards" : cards})
    final.update({"Character Talents" : cards})
    final.update({"Unsorted" : rest})
    with open(IMP, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def Main():
    extractor()