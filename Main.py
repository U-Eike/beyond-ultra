import Data_Remapper
import ArtifactMap
import Stage_Mapper
import CharacterMap
import WeaponMap
import ItemMap
import Final
import TextMapper
import QuestMap
import MonsterMapper
import MechanicusMapper

def Main():
    """
    Data_Remapper.Main()
    Stage_Mapper.Main()
    ItemMap.Main()
    CharacterMap.Main()
    WeaponMap.Main()
    ArtifactMap.Main()
    """
    Final.main()
Main()