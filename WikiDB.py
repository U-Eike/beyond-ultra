import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
DB = os.path.join(mydir, "Final")
EndPath = os.path.join(Bot_Files, "Tags.json")

def Botloader(Loc):
    Loc = os.path.join(Bot_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

QuestMap = Botloader("QuestMap.json")
DD = RMaploader("DialogueData.json")
ND = RMaploader("NPCData.json")
QChats = Outloader("RawDiag.json")
LangEN = RMaploader("TextEN.json")

def repla(st):
    st = st.replace("<color=#FF9999FF>","**")##Pyro Replace
    st = st.replace("<color=#FFE14BFF>","**")##Null Replace
    st = st.replace("<color=#80FFD7FF>","**")##Anemo Replace
    st = st.replace("<color=#FFD780FF>","**")##Anemo Replace
    st = st.replace("<color=#99FFFFFF>","**")##Cryo Replace
    st = st.replace("<color=#80C0FFFF>","**")##Hydro Replace
    st = st.replace("<color=#FFACFFFF>","**")##Electro Replace
    st = st.replace("<color=#FFE699FF>","**")##Geo Replace
    st = st.replace("<i>","_")##Italic Replace
    st = st.replace("</i>","_")##Italic Replace
    st = st.replace("<color=#FFD780FF>","**")##Unknown Replace
    st = st.replace("\u00b7","\t-")##Bullet Point Replace
    st = st.replace("\\n","\n")##Double Tab Replace
    st = st.replace("\\\\n","\\n")##Double Tab Replace
    st = st.replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}","Tap")##Hold Prompt Replace
    st = st.replace("#","")##Italic Replace
    st = st.replace("</color>","**")##Color End Replace
    return st

def qrepla(st):
    st = st.replace("<color=#FF9999FF>","<strong>")##Pyro Replace
    st = st.replace("<color=#FFE14BFF>","<strong>")##Null Replace
    st = st.replace("<color=#80FFD7FF>","<strong>")##Anemo Replace
    st = st.replace("<color=#FFD780FF>","<strong>")##Anemo Replace
    st = st.replace("<color=#99FFFFFF>","<strong>")##Cryo Replace
    st = st.replace("<color=#80C0FFFF>","<strong>")##Hydro Replace
    st = st.replace("<color=#00E1FFFF>","<strong>")##Hydro Replace
    st = st.replace("<color=#FFACFFFF>","<strong>")##Electro Replace
    st = st.replace("<color=#FFACFFFF>","<strong>")##Electro Replace
    st = st.replace("<color=#FFE699FF>","<strong>")##Geo Replace
    st = st.replace("{NICKNAME}","[Player]")##Replace{Mamigo}{Famiga}
    st = st.replace("{M#amigo}{F#amiga}","[amigo/amiga]")##Replace
    st = st.replace("{M#o}{F#a}","[o/a]")##Replace
    st = st.replace("{F#brother}{M#sister}","[brother/sister]")##Replace
    st = st.replace("{M#sister}{F#brother}","[sister/brother]")##Replace
    st = st.replace("{M#his}{F#hers}","[his/hers]")##Replace
    st = st.replace("{F#big sis}{M#big bro}","big [sis/bro]")##Replace
    st = st.replace("{M#Mr.}{F#Ms.}","[Mr./Mrs.]")##Replace
    st = st.replace("{F#Aether}{M#Lumine}","[Aether/Lumine]")##Replace
    st = st.replace("{F#Lumine}{M#Aether}","[Lumine/Aether]")##Replace
    st = st.replace("{F#Miss}{M#Mr.}","[Miss/Mr.]")##Replace
    st = st.replace("{M#Mr.}{F#Miss}","[Mr./Miss]")##Replace
    st = st.replace("{M#Sir}{F#Ma'am}","[Sir/Ma'am]")##Replace
    st = st.replace("{M#sir}{F#ma'am}","[sir/ma'am]")##Replace
    st = st.replace("{F#His Highness, the Prince}{M#Her Highness, the Princess}","[His Highness, the Prince/Her Highness, the Princess]")##Replace
    st = st.replace("{F#lady}{M#gentleman}","[lady/gentleman]")##Replace
    st = st.replace("{M#him}{F#her}","[him/her]")##Replace
    st = st.replace("{F#her brother}{M#his sister}","[her brother/his sister]")##Replace
    st = st.replace("{F#him \"The Prince.\"}{M#her \"The Princess.\"}","\"The [Prince/Princess]\"")##Replace
    st = st.replace("{F#Prince...}{M#Princess...}","[Prince/Princess]...")##Replace
    st = st.replace("{F#Prince}{M#Princess}","[Prince/Princess]")##Replace
    st = st.replace("{F#she's}{M#he's}","[she's/he's]")##Replace
    st = st.replace("{F#he's}{M#she's}","[he's/she's]")##Replace
    st = st.replace("{F#he}{M#that she}","that [he/she]")##Replace
    st = st.replace("{M#She}{F#He}","[She/He]")##Replace
    st = st.replace("{F#He}{M#She}","[He/She]")##Replace
    st = st.replace("{F#her}{M#his}","[her/his]")##Replace
    st = st.replace("{F#his}{M#her}","[his/her]")##Replace
    st = st.replace("{M#her}{F#his}","[her/his]")##Replace
    st = st.replace("{PLAYERAVATAR#SEXPRO[INFO_MALE_PRONOUN_BIGBROTHER|INFO_FEMALE_PRONOUN_BIGSISTER]}","Big [brother/sister]")##Replace
    st = st.replace("{M#his}{F#her}","[his/her]")##Replace
    st = st.replace("{M#him}{F#her}","[him/her]")##Replace
    st = st.replace("{F#him}{M#her}","[him/her]")##Replace
    st = st.replace("{F#she'll}{M#he'll}","[she'll/he'll]")##Replace
    st = st.replace("{F#she}{M#he}","[she/he]")##Replace
    st = st.replace("{F#Show him}{M#Well then, show her}","Well then, show [him/her]")##Replace
    st = st.replace("{F#woman}{M#man}","[woman/man]")##Replace
    st = st.replace("{F#the monsters}{M#monsters}","the monsters")##Replace
    st = st.replace("{M#Traveler guy}{F#Traveler lady}","Traveler [guy/lady]")##Replace
    st = st.replace("{F#your brother}{M#your sister}","your [brother/sister]")##Replace
    st = st.replace("{F#my brother}{M#Lumine}","[my brother/my sister]")##Replace
    st = st.replace("{F#your brother \"Prince.\"}{M#your sister \"Princess.\"}","your [brother \"Prince.\"/sister \"Princess.\"]")##Replace
    st = st.replace("{M#boy}{F#girl}","[boy/girl]")##Replace
    st = st.replace("{M#mister}{F#lady}","[mister/lady]")##Replace
    st = st.replace("{M#guy}{F#lady}","[guy/lady]")##Replace
    st = st.replace("{M#Mr. Nice Guy}{F#Ms. Nice Lady}","[Mr. Nice Guy/Ms. Nice Lady]")##Replace
    st = st.replace("{F#Madam}{M#Sir}","[Madam/Sir]")##Replace
    st = st.replace("{M#HE}{F#SHE}","[HE/SHE]")##Replace
    st = st.replace("{M#himself}{F#herself}","[himself/herself]")##Replace
    st = st.replace("{M#He}{F#She}","[He/She]")##Replace
    st = st.replace("{M#he}{F#she}","[he/she]")##Replace
    st = st.replace("{M#she}{F#he}","[she/he]")##Replace
    st = st.replace("{F#he}{M#she}","[he/she]")##Replace
    st = st.replace("{F#He}{M#She}","[He/She]")##Replace
    st = st.replace("{M#nice man}{F#nice lady}_x000D_","nice [man/lady]")##Replace
    st = st.replace("{M#her}{F#him}","[her/him]")##Replace
    st = st.replace("{F#her}{M#him}","[her/him]")##Replace
    st = st.replace("{M#man}{F#lady}","[man/lady]")##Replace
    st = st.replace("<i>","<em>")##Italic Replace{
    st = st.replace("</i>","</em>")##Italic Replace
    st = st.replace("<color=#FFD780FF>","**")##Unknown Replace
    st = st.replace("\u00b7","\t-")##Bullet Point Replace
    st = st.replace("\\n","\n")##Double Tab Replace
    st = st.replace("\\\\n","\\n")##Double Tab Replace
    st = st.replace("{LAYOUT_MOBILE#Tap}{LAYOUT_PC#Press}{LAYOUT_PS#Press}","Tap")##Hold Prompt Replace
    st = st.replace("#","")##Italic Replace
    st = st.replace("</color>","</strong>")##Color End Replace
    return st

def freog(rec):
    if len(rec) == 1:
        nam = rec[0]["Name"]
    elif len(rec) == 3:
        nrec = [[],[],[]]
        for var in rec:
            if var["Qua"] == "FOOD_QUALITY_ORDINARY":
                nrec[0] = var
            elif var["Qua"] == "FOOD_QUALITY_STRANGE":
                nrec[1] = var
            elif var["Qua"] == "FOOD_QUALITY_DELICIOUS":
                nrec[2] = var
            rec = nrec
        nam = rec[0]["Name"]
    elif len(rec) == 4:
        nrec = [[],[],[],[]]
        for var in rec:
            if var["Qua"] == "FOOD_QUALITY_ORDINARY":
                nrec[0] = var
            elif var["Qua"] == "FOOD_QUALITY_STRANGE":
                nrec[1] = var
            elif var["Qua"]== "FOOD_QUALITY_DELICIOUS":
                nrec[2] = var
            elif var["Qua"] == "Specialty":
                nrec[3] = var
            rec = nrec
        nam = rec[0]["Name"]
    else:
        nam = ""
    return [rec,nam]

def conv(st,lang=LangEN):
    st = str(st)
    if st in [""]:
        return ""
    elif st in ["QUALITY_BLUE"]:
        return "3"
    elif st in ["QUALITY_PURPLE"]:
        return "4"
    elif st in ["QUALITY_ORANGE"]:
        return "5"
    elif st in lang.keys():
        return repla(lang[st])
    else:
        print(st)
        return("")

def qconv(st,lang=LangEN):
    st = str(st)
    if st in [""]:
        return ""
    elif st in ["QUALITY_BLUE"]:
        return "3"
    elif st in ["QUALITY_PURPLE"]:
        return "4"
    elif st in ["QUALITY_ORANGE"]:
        return "5"
    elif st in lang.keys():
        return qrepla(lang[st])
    else:
        print(st)
        return("")

def delver(Hole,lan,dep=0,order = -1):
    End = []
    order+=1
    #if type(Hole) == int:
        #Hole = str(Hole)
    for item in Hole:
        if type(item) == list:
            TempEnd = []
            for Branch in item:
                Tdep = dep + 1
                #If Nested List Add To Temp List
                if type(item) == list:
                    TempEnd.append(delver(Branch,lan,Tdep))
                #Add To Return List
                else:
                    End.append(delver(Branch,lan,Tdep))
            #If The Longest Depth Of The Nested List Is 1 (Aka Single Choices)
            if longest(TempEnd) == 1 and len(TempEnd) > 1:
                #Table Head
                TT = "<br /><table align=\"center\" border=\"2\" cellpadding=\"8\"><thead><tr>"
                TH = []
                #Append Items To Table Head
                for item in TempEnd:
                    #Pop Table Head From Nested List
                    TT += "<th scope=\"col\" style=\"text-align:center; vertical-align:middle\">- "+item.pop(0).split(": ")[1]+"</th>"
                TT += "</tr></thead></table><br />"
                #Add Table Head To String
                End.append(TT)
            #Else
            elif longest(TempEnd) > 1 and len(TempEnd) > 1:
                #Table Head
                TT = "<br /><table align=\"center\" border=\"2\" cellpadding=\"8\"><thead><tr>"
                TH = []
                #Append Items To Table Head
                for item in TempEnd:
                    #Pop Table Head From Nested List
                    TT += "<th scope=\"col\" style=\"text-align:center; vertical-align:middle\">- "+item.pop(0).split(": ")[1]+"</th>"
                #print([TempEnd])
                TT += "</tr></thead>"
                TT += "<tbody><tr>"
                #Append Items To Table Head
                for item in TempEnd:
                    #Table Body Consists Of Leftovers From Nested List
                    TT += "<td style=\"text-align:left; vertical-align:top\">"+"<br />".join(item)+"</td>"
                TT += "</tr></tbody></table><br />"
                #Add Table Head And Body To String
                End.append(TT)
                
        else:
            DDL = DD[str(item)]
            NPCN = DDL["TalkRole"]
            if NPCN["Id"] in ["","主角","玩家","0","****"]:
                if "Type" in NPCN:
                    if NPCN["Type"] == "TALK_ROLE_MATE_AVATAR":
                        Name = "Sibling"
                    else:
                        Name = "Traveler"
                else:
#            if NPCN["Type"] in ["TALK_ROLE_PLAYER"]:
                    Name = "Traveler"
            else:
                Name = qconv(ND[NPCN["Id"]]["NameTextMapHash"],lan)
            End.append("<strong>"+Name+"</strong> : "+qconv(DDL["TalkContentTextMapHash"],lan))
    if dep == 0:
        End = "<br />".join(End)
    return(End)

def longest(lis):
    mx = len(lis[0])
    for item in lis:
        if len(item) > mx:
            mx = len(item)
    return mx

def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret

def QuestMapper(lan=LangEN,Out="QuestPaths.json"):
    fin = {}
    for MQuest in QuestMap:
        CQ = QuestMap[MQuest]
        temp = {}
        temp.update({"Name" : qconv(CQ["Title"])})
        temp.update({"Desc" : qconv(CQ["Desc"])})
        TTemp = []
        STemp = {}
        for Stage in CQ["Stages"]:

            STemp.update({"0" : "<h5 class=\"decorativeHeadline\"><strong>〓Dialogue〓</strong></h5><p>"})
            CTemp = []
            CID = CQ["Stages"][Stage]["DiD"]
            if CID not in QChats:
                continue
            CTemp = delver(QChats[CID],lan)
            """
            for chat in QChats[CID]:
                if type(chat) == list:
                    break
                DDL = DD[chat]
                CTemp.append(conv(DDL["TalkContentTextMapHash"]))
            """
            try:
                STemp.update({Stage : "".join(CTemp)})
                TTemp.append("".join(CTemp))
            except:
                STemp.update({Stage : CTemp})
                TTemp.append(CTemp)
        Tit = qconv(CQ["Title"]).replace("?","").replace("\"","").replace(":","")
        ETemp = ""
        if qconv(CQ["Title"]) == "":
            Tit = MQuest
        if TTemp != "":
            with open("WikiDB\\Quests\\"+Tit+".txt", "w", encoding='utf-8') as write_file:
                write_file.write("<hr /><h3>Next Stage</h3><hr /></br>".join(TTemp))
        STemp.update({"999" : "End"})
        temp.update({"Flow" : STemp})

        fin.update({MQuest :  temp  })

    with open(Out, "w", encoding='utf-8') as write_file:
        json.dump(fin, write_file, indent=4, ensure_ascii=False)

""" TT = delver(QChats["1800001"],LangEN)
with open("Test.json", "w") as write_file:
    json.dump(TT, write_file, indent=4) """
#print(TT)
QuestMapper()