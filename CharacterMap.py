import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
langpath = os.path.join(Remapped_Data, "TextEN.json")
Itemdex = os.path.join(Out_Files, "ItemEN.json")
Charm = os.path.join(Bot_Files, "CharMap.json")


def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)
#Weapon Files
AD = RMaploader("AvatarData.json")
ASDD = RMaploader("AvatarSkillDepotData.json")
ASD = RMaploader("AvatarSkillData.json")
ATD = RMaploader("AvatarTalentData.json")
FSD = RMaploader("FetterStoryData.json")
FID = RMaploader("FetterInfoData.json")
PSD = RMaploader("ProudSkillData.json")
APD = RMaploader("AvatarPromoteData.json")
lang = RMaploader("TextEN.json")
ItemEN = Outloader(Itemdex)
Hak = Outloader("CharTables.json")
NT = Outloader("NormalTables.json")
ST = Outloader("SkillTables.json")
BT = Outloader("BurstTables.json")


def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret


def extractor():
    final = {}
    for item in AD:
        temp = {}
        SNam = AD[item]["IconName"].split("_")[-1]
        temp.update({"Helper" : SNam})
        temp.update({"Name" : AD[item]["NameTextMapHash"]})
        temp.update({"Weapon" : AD[item]["WeaponType"]})
        temp.update({"Rarity" : AD[item]["QualityType"]})
        temp.update({"Short" : AD[item]["DescTextMapHash"]})
        temp.update({"Icon" : SNam})
        SDID = str(AD[item]["SkillDepotId"])
        EDID = str(AD[item]["AvatarPromoteId"])
        Evol = []
        Total = {}
        for level in APD[EDID][1:]:
            lv = level
            Evol.append([lv["ScoinCost"],lv["CostItems"]])

        for level in Evol:
            for mat in level[1]:
                if mat["Id"] in Total:
                    Total[mat["Id"]]+=mat["Count"]
                else:
                    Total.update({mat["Id"] : mat["Count"]})

        TKeys = list(Total.keys())
        if len(TKeys) == 9:
            Total = {  TKeys[0] : Total[TKeys[0]],
                        TKeys[3] : Total[TKeys[3]],
                        TKeys[6] : Total[TKeys[6]],
                        TKeys[8] : Total[TKeys[8]],
                        TKeys[2] : Total[TKeys[2]],
                        TKeys[5] : Total[TKeys[5]],
                        TKeys[7] : Total[TKeys[7]],
                        TKeys[1] : Total[TKeys[1]],
                        TKeys[4] : Total[TKeys[4]]
            }
        temp.update({"Ascension" : Evol})
        temp.update({"total" : Total})
        temp.update({"Normal Attack" : NT[item]})
        temp.update({"Elemental Skill" : ST[item]})
        temp.update({"Elemental Burst" : BT[item]})

        Talent = {}
        try:
            for TalNum in [1,2,3,4,5,6]:
                TID = ASDD[SDID]["Talents"][TalNum-1]
                temptal = {}
                temptal.update({"Name" : ATD[str(TID)]["NameTextMapHash"]})
                temptal.update({"Description" : ATD[str(TID)]["DescTextMapHash"]})
                temptal.update({"Icon" : ATD[str(TID)]["Icon"]})
                Talent.update({str(TalNum) : temptal})

        except:
            for TalNum in [1,2,3,4,5,6]:
                temptal = {}
                temptal.update({"Name" : ""})
                temptal.update({"Description" : ""})
                temptal.update({"Icon" : ""})
                Talent.update({str(TalNum) : temptal})
        temp.update({"Talents" : Talent})
        passives = []

        for pas in ASDD[SDID]["InherentProudSkillOpens"]:
            try:
                passives.append(pas["ProudSkillGroupId"])
            except:
                passives.append("")
        cpass = []
        for GID in passives:
            try:
                cpass.append([str(PSD[str(GID)][0]["NameTextMapHash"]),str(PSD[str(GID)][0]["DescTextMapHash"]),str(PSD[str(GID)][0]["Icon"])])
            except:
                cpass.append(["","",""])
        temp.update({"Passives" : cpass})

        story = {}
        emp = {"ID" : "","Name" : "","Description" : ""}
        try:
            story.update({"0" : FSD[str(item)][0]})
        except:
            story.update({"0" : emp})
        try:
            story.update({"1" : FSD[str(item)][1]})
        except:
            story.update({"1" : emp})
        try:
            story.update({"2" : FSD[str(item)][2]})
        except:
            story.update({"2" : emp})
        try:
            story.update({"3" : FSD[str(item)][3]})
        except:
            story.update({"3" : emp})
        try:
            story.update({"4" : FSD[str(item)][4]})
        except:
            story.update({"4" : emp})
        try:
            story.update({"5" : FSD[str(item)][5]})
        except:
            story.update({"5" : emp})
        try:
            story.update({"Fave" : FSD[str(item)][6]})
        except:
            story.update({"Fave" : emp})
        try:
            story.update({"Focus" : FSD[str(item)][7]})
        except:
            story.update({"Focus" : emp})

        temp.update({"Story" : story})
        
        try:
            temp.update({"Origin" : FID[str(item)]["Origin"]})
            temp.update({"Element" : FID[str(item)]["Element"]})
            temp.update({"Constellation" : FID[str(item)]["Constellation"]})
            temp.update({"Title" : FID[str(item)]["Title"]})
            temp.update({"Intro" : FID[str(item)]["Intro"]})
        except:
            temp.update({"Origin" : ""})
            temp.update({"Element" : ""})
            temp.update({"Constellation" : ""})
            temp.update({"Title" : ""})
            temp.update({"Intro" : ""})
        stats = []
        for level in [0,1,20,21,41,42,52,53,63,64,74,75,85,86,96]:
            stats.append(Hak[item]["Stats"][level])
        temp.update({"Stats" : stats})
    
        final.update({item : temp})
    with open(Charm, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def Main():
    extractor()
