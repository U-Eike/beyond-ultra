import sys
import os
from pathlib import Path
import json
import math
import shutil
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
from PIL import ImageOps

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Bot_Files = os.path.join(mydir, "Bot_Files")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
mydir = os.path.join(mydir, "Image Hell")
F1 = os.path.join(mydir, "1st_Circle")
S2 = os.path.join(mydir, "2nd_Circle")
T3 = os.path.join(mydir, "3rd_Circle")
F4 = os.path.join(mydir, "4th_Circle")
F5 = os.path.join(mydir, "5th_Circle")
F5C = os.path.join(mydir, "5th_Circle","Characters")
Earth = os.path.join(mydir, "Earth")

def Botloader(Loc):
    Loc = os.path.join(Bot_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

LangEN = RMaploader("TextEN.json")
TagMap = Botloader("Tags.json")

def conv(st):
    st = str(st)
    if st == "":
        return ""
    elif st in LangEN.keys():
        return LangEN[st]
    #elif st in SP.keys():
        #return SP[st]["Story"]
    else:
#        print(st)
        return("")

CharMap = Botloader("CharMap.json")
ArtifMap = Botloader("ArtifMap.json")
WeapMap = Botloader("WeapMap.json")
ItemMap = Botloader("ItemMap.json")

def Third():
    for item in os.listdir(F1):
        Work = item.split("_")
        F1N = os.path.join(mydir, "1st_Circle",item)
        S2N = os.path.join(mydir, "2nd_Circle",item)
        if Work[0] == "UI":
            if Work[1] == "ItemIcon":
                if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1])):
                    pass
                else:
                    os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1]))
                if len(Work) == 4:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],(Work[2]+"_"+Work[3]))
                else:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])
                shutil.copyfile(F1N, T3N)
            if Work[1] == "AvatarIcon":
                if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1])):
                    pass
                else:
                    os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1]))
                if len(Work) == 4:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],(Work[2]+"_"+Work[3]))
                else:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])
                shutil.copyfile(F1N, T3N)
            if Work[1] == "EquipIcon":
                if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1])):
                    pass
                else:
                    os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1]))
                if len(Work) >= 4:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],"_".join(Work[2:]))
                else:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])
                shutil.copyfile(F1N, T3N)
            if Work[1] == "NameCardIcon":
                if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1])):
                    pass
                else:
                    os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1]))
                if len(Work) >= 4:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],"_".join(Work[2:]))
                else:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])
                shutil.copyfile(F1N, T3N)
            if Work[1] == "MonsterIcon":
                if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1])):
                    pass
                else:
                    os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1]))
                if len(Work) >= 4:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],"_".join(Work[2:]))
                else:
                    T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])
                shutil.copyfile(F1N, T3N)
            if Work[1] == "Talent":
                if Work[2] == "C":
                    if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])):
                        pass
                    else:
                        os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2]))
                    if len(Work) >= 4:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],"_".join(Work[3:]))
                    else:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3])
                    shutil.copyfile(F1N, T3N)
            if Work[1] == "Gacha":
                if Work[2] == "AvatarImg":
                    if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])):
                        pass
                    else:
                        os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2]))
                    if len(Work) >= 4:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],"_".join(Work[3:]))
                    else:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3])
                    shutil.copyfile(F1N, T3N)
                if Work[2] == "AvatarIcon":
                    if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2])):
                        pass
                    else:
                        os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2]))
                    if len(Work) >= 4:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],"_".join(Work[3:]))
                    else:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3])
                    shutil.copyfile(F1N, T3N)
                if Work[2] == "EquipIcon":
                    if os.path.exists(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3])):
                        pass
                    else:
                        os.makedirs(os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3]))
                    if len(Work) >= 4:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3],"_".join(Work[4:]))
                    else:
                        T3N = os.path.join(mydir, "3rd_Circle",Work[0],Work[1],Work[2],Work[3],Work[4])
                    shutil.copyfile(F1N, T3N)
        #print(item)
WO = os.path.join(F4, "Weapons")
IO = os.path.join(F4, "Items")
FO = os.path.join(F4, "Food")
for item in [WO,IO,FO]:
    if os.path.exists(item):
        pass
    else:
        os.makedirs(item)

def Fourth():
    for Weap in WeapMap:
        if TagMap["Weapons"][Weap]["Ban"]:
            continue
        item = WeapMap[Weap]
        Norm = os.path.join(F1,item["Icon"]+".png")
        Awk = os.path.join(F1,item["AIcon"]+".png")
        Name = conv(item["Name"]).replace(" ","_")
        NormO = ExCI(os.path.join(F5,"Weapons",Name),"Base.png")
        AwkO = ExCI(os.path.join(F5,"Weapons",Name),"Awk.png")
        try:
            shutil.copyfile(Norm, NormO)
            shutil.copyfile(Awk, AwkO)
        except:
            print(Name+" Missing")


    for Mat in ItemMap["Materials"]:
        if TagMap["Items"][Mat]["Ban"]:
            continue
        item = ItemMap["Materials"][Mat]
        Norm = os.path.join(F1,item["Icon"]+".png")
        Name = conv(item["Item Name"]).replace(" ","_")
        NormO = ExCI(os.path.join(F5,"Mats",Name),"Raw.png")
        try:
            shutil.copyfile(Norm, NormO)
        except:
            print(Name+" Missing")

    for Mat in ItemMap["Ascension Materials"]:
        for tier in ItemMap["Ascension Materials"][Mat]:
            if TagMap["Items"][tier]["Ban"]:
                continue
            item = ItemMap["Ascension Materials"][Mat][tier]
            Norm = os.path.join(F1,item["Icon"]+".png")
            Name = conv(item["Item Name"]).replace(" ","_")
            NormO = ExCI(os.path.join(F5,"Mats",Name),"Raw.png")
            try:
                shutil.copyfile(Norm, NormO)
            except:
                print(Name+" Missing")
    
    for Mat in ItemMap["Food"]:
        if TagMap["Items"][Mat]["Ban"]:
            continue
        item = ItemMap["Food"][Mat]
        Norm = os.path.join(F1,item["Icon"]+".png")
        Name = conv(item["Item Name"]).replace(" ","_").replace("\"","").replace("\\","")
        NormO = ExCI(os.path.join(F5,"Food",Name),"Raw.png")
        try:
            shutil.copyfile(Norm, NormO)
        except:
            print(Name+" Missing")

OO = os.path.join(F4, "Food")
XFP = os.path.join(Earth, "XF.png")
XF = Image.open(XFP).convert("RGBA")
UP = os.path.join(F4, "Test")
Charp = os.path.join(Earth, "Characters\\Side")
X1 = XF.resize((256,312))



Siz = (100,100)
BGSiz = (100,122)
Temp = os.path.join(Earth, "BGC\\5.png")
Temp = Image.open(Temp).convert("RGBA")
Star_5 = Temp.resize(Siz)

Temp = os.path.join(Earth, "BGC\\4.png")
Temp = Image.open(Temp).convert("RGBA")
Star_4 = Temp.resize(Siz)

Temp = os.path.join(Earth, "BGC\\3.png")
Temp = Image.open(Temp).convert("RGBA")
Star_3 = Temp.resize(Siz)

Temp = os.path.join(Earth, "BGC\\2.png")
Temp = Image.open(Temp).convert("RGBA")
Star_2 = Temp.resize(Siz)

Temp = os.path.join(Earth, "BGC\\1.png")
Temp = Image.open(Temp).convert("RGBA")
Star_1 = Temp.resize(Siz)


Temp = os.path.join(Earth, "BGC\\0.png")
Temp = Image.open(Temp).convert("RGBA")
Star_0 = Temp.resize(Siz)

RarBG = [Star_0,Star_1,Star_2,Star_3,Star_4,Star_5]

Temp = os.path.join(Earth, "BGC\\BG.png")
Temp = Image.open(Temp).convert("RGBA")
Item_BG = Temp.resize(BGSiz)


Temp = os.path.join(Earth, "BGC\\WBG.png")
Temp = Image.open(Temp).convert("RGBA")
Weap_BG = Temp

Temp = os.path.join(Earth, "BGC\\Curve.png")
Temp = Image.open(Temp).convert("RGBA")
Curve_M = Temp.resize(Siz)

Temp = os.path.join(Earth, "BGC\\Crest.png")
Crest = Image.open(Temp).convert("RGBA")

Norm = os.path.join(IO,"Brilliant_Diamond_Gemstone"+".png")

def ExC(Loc):
    if os.path.exists(Loc):
        pass
    else:
        os.makedirs(Loc)
    return(Loc)

def ExCI(Loc,Nam):
    if os.path.exists(Loc):
        pass
    else:
        os.makedirs(Loc)
    return(os.path.join(Loc,Nam))

def ICards():
    for Mat in ItemMap["Materials"]:

        if TagMap["Items"][Mat]["Ban"] or Mat in ["100757","100758"]:
            continue
        item = ItemMap["Materials"][Mat]
        Norm = os.path.join(F1,item["Icon"]+".png")
        Name = conv(item["Item Name"]).replace(" ","_")
        NormO = os.path.join(IO,Name+".png")
        SID = item["Rarity"]
        Img = Image.open(NormO).convert("RGBA")
        Img = Img.resize(Siz)

        Item_BG.paste(RarBG[SID],mask = Curve_M)
        Item_BG.paste(Crest,mask = Crest)
        Item_BG.paste(Img,mask = Img)
        OP = ExCI(os.path.join(F5,"Mats",Name),"Card.png")
        Item_BG.save(OP)

    for Mat in ItemMap["Ascension Materials"]:
        for tier in ItemMap["Ascension Materials"][Mat]:
            item = ItemMap["Ascension Materials"][Mat][tier]
            if TagMap["Items"][tier]["Ban"] or tier in ["100757","100758"]:
                continue

            Norm = os.path.join(F1,item["Icon"]+".png")
            Name = conv(item["Item Name"]).replace(" ","_")
            NormO = os.path.join(IO,Name+".png")
            SID = item["Rarity"]

            Img = Image.open(NormO).convert("RGBA")
            Img = Img.resize(Siz)

            Item_BG.paste(RarBG[SID],mask = Curve_M)
            Item_BG.paste(Crest,mask = Crest)
            Item_BG.paste(Img,mask = Img)
            OP = ExCI(os.path.join(F5,"Mats",Name),"Card.png")
            Item_BG.save(OP)


font = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 16)
Bfont = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 60)
Sfont = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 58)

def CharImD():
    print("Workin")
    for Char in CharMap:

        CM = CharMap[Char]
        CID = TagMap["Characters"][Char]["Name"]        
#        CD = os.path.join(FO,CID+".png")
        CD = os.path.join(F5C,CID)
        if Char+".png" not in os.listdir(Charp):
            continue
        #print(CID)
        ExC(CD)
        C = 0
        TP = ExC(os.path.join(F5C,CID,"Passives"))
        for const in CM["Passives"]:
            OP = os.path.join(F1,const[2]+".png")
            FP = os.path.join(TP,str(C)+".png")
            try:
                shutil.copyfile(OP, FP)
            except:
                print(const[2]+" Missing")
            C+=1

        C = 1
        TP = ExC(os.path.join(F5C,CID,"Talents"))
        for Tal in CM["Talents"]:
            Tal = CM["Talents"][Tal]
            OP = os.path.join(F1,Tal["Icon"]+".png")
            FP = os.path.join(TP,str(C)+".png")
            try:
                shutil.copyfile(OP, FP)
            except:
                print(Tal["Icon"]+" Missing")
            C+=1

        C = 0
        TP = ExC(os.path.join(F5C,CID,"Skills"))
        for SI in [CM["Normal Attack"]["Img"],CM["Elemental Skill"]["Img"],CM["Elemental Burst"]["Img"],]:
            OP = os.path.join(F1,SI+".png")
            if C == 2:
                OP = os.path.join(F1,SI+"_HD.png")               
            FP = os.path.join(TP,str(C)+".png")
            try:
                shutil.copyfile(OP, FP)
            except:
                print(SI+" Missing")
            C+=1
        TP = ExC(os.path.join(F5C,CID))
        OP = os.path.join(F1,"UI_Gacha_AvatarIcon_"+CM["Icon"]+".png")
        FP = os.path.join(TP,"Raw_Panel.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_Gacha_AvatarIcon_"+CM["Icon"]+".png"+" Missing")

        OP = os.path.join(F1,"UI_Gacha_AvatarImg_"+CM["Icon"]+".png")
        FP = os.path.join(TP,"Raw_Full.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_Gacha_AvatarImg_"+CM["Icon"]+".png"+" Missing")

        OP = os.path.join(F1,"UI_AvatarIcon_"+CM["Icon"]+".png")
        FP = os.path.join(TP,"Raw_Icon.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_AvatarIcon_"+CM["Icon"]+".png"+" Missing")

        OP = os.path.join(F1,"UI_LegendQuestImg_"+CM["Icon"]+".png")
        FP = os.path.join(TP,"Raw_Page.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_LegendQuestImg_"+CM["Icon"]+".png"+" Missing")

        OP = os.path.join(F1,"UI_NameCardIcon_"+CM["Icon"]+".png")
        FP = os.path.join(TP,"Raw_Card.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_NameCardIcon_"+CM["Icon"]+".png"+" Missing")

        OP = os.path.join(F1,"UI_AvatarIcon_"+CM["Icon"]+"_Card.png")
        FP = os.path.join(TP,"Raw_Item.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_AvatarIcon_"+CM["Icon"]+"_Card.png"+" Missing")
            
        OP = os.path.join(F1,"UI_NameCardPic_"+CM["Icon"]+"_Alpha.png")
        FP = os.path.join(TP,"Raw_Bar.png")
        try:
            shutil.copyfile(OP, FP)
        except:
            print("UI_NameCardPic_"+CM["Icon"]+"_Alpha.png"+" Missing")
            
def CharAscIgs():
    XOff = 350
    YOff = 100
    TXOff = XOff+45
    TYOff = 203

    Card = os.path.join(Earth, "BGC\\Card.png")
    Card = Image.open(Card).convert("RGBA").resize((327,1324))

    StarBG = os.path.join(Earth, "BGC\\AStar.png")
    StarBG = Image.open(StarBG).convert("RGBA").resize((100,122))
    print("Workin")

    for Char in CharMap:

        if TagMap["Characters"][Char]["Ban"]:
            continue
        Side = os.path.join(Charp,Char+".png")
        Side = Image.open(Side).convert("RGBA").resize((320,1024))
        Base = os.path.join(Earth, "BGC\\"+LangEN[str(CharMap[Char]["Element"])]+".png")
        Base = Image.open(Base).convert("RGBA").resize((1124,1024))
        MatNums = ImageDraw.Draw(Base)
        yCount = 0

        for rank in CharMap[Char]["Ascension"]:
            xCount = 0
            for item in rank[1]:
                temp = os.path.join(UP,item["Id"].replace(" ","_")+".png")
                temp = Image.open(temp).convert("RGBA")
                if yCount==0:
                    Base.paste(temp,(XOff+75+(150*xCount),YOff+(150*yCount)),mask = temp)
                else:
                    Base.paste(temp,(XOff+(150*xCount),YOff+(150*yCount)),mask = temp)

                if item["Count"] < 10:
                    if yCount==0:
                        MatNums.text((TXOff+75+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                    else:
                        MatNums.text((TXOff+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                else:
                    MatNums.text((TXOff-4+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                    
                xCount+=1

            if yCount==0:
                xCount+=1
            Base.paste(StarBG,(XOff+(150*xCount),YOff+(150*yCount)),mask = StarBG)
            if yCount==0:
                MatNums.text((TXOff-10+(150*xCount),YOff+25+(150*yCount)), str(yCount+1),font = Bfont, fill=(0,0,0))
            else:
                MatNums.text((TXOff-15+(150*xCount),YOff+25+(150*yCount)), str(yCount+1),font = Bfont, fill=(0,0,0))

            yCount+=1
        Base.paste(Card,(-5,-100),mask = Card)
        Base.paste(Side,mask = Side)
        OP = ExCI(os.path.join(F5,"Characters\\"+TagMap["Characters"][Char]["Name"]),"ASC.png")
        Base.save(OP)

        print(Char)

def CharTalAscIgs():
    XOff = 350
    YOff = 100
    TXOff = XOff+45
    TYOff = 203

    Card = os.path.join(Earth, "BGC\\Card.png")
    Card = Image.open(Card).convert("RGBA").resize((327,1324))

    StarBG = os.path.join(Earth, "BGC\\AStar.png")
    StarBG = Image.open(StarBG).convert("RGBA").resize((100,122))

    Panel = os.path.join(Earth, "BGC\\StarBG.png")
    Panel = Image.open(Panel).convert("RGBA").resize((700,244+50))
    print("Workin")

    for Char in CharMap:

        if TagMap["Characters"][Char]["Ban"]:
            continue
        Side = os.path.join(Charp,Char+".png")
        Side = Image.open(Side).convert("RGBA").resize((320,1024))
        Base = os.path.join(Earth, "BGC\\"+LangEN[str(CharMap[Char]["Element"])]+".png")
        Base = Image.open(Base).convert("RGBA").resize((1550,1024))
        MatNums = ImageDraw.Draw(Base)
        yCount = 0
        TalNum = 0
        P2 = 0
        for rank in CharMap[Char]["Normal Attack"]["Upgrade"]:
            if TalNum < 5:
                xCount = 0
                for item in rank[1]:
                    temp = os.path.join(UP,item["Id"].replace(" ","_")+".png")
                    temp = Image.open(temp).convert("RGBA")
                    Base.paste(temp,(XOff+(150*xCount),YOff+(150*yCount)),mask = temp)

                    if item["Count"] < 10:
                        MatNums.text((TXOff+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                    else:
                        MatNums.text((TXOff-4+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                        
                    xCount+=1

                if yCount<=4:                
                    xCount+=1
                Base.paste(StarBG,(XOff+(150*2),YOff+(150*yCount)),mask = StarBG)
                if yCount==0:
                    MatNums.text((TXOff-10+(150*2),YOff+25+(150*yCount)), str(TalNum+2),font = Bfont, fill=(0,0,0))
                else:
                    MatNums.text((TXOff-15+(150*2),YOff+25+(150*yCount)), str(TalNum+2),font = Bfont, fill=(0,0,0))

                yCount+=1
            elif TalNum < 8:
                xCount = 0
                for item in rank[1]:
                    temp = os.path.join(UP,item["Id"].replace(" ","_")+".png")
                    temp = Image.open(temp).convert("RGBA")
                    Base.paste(temp,(XOff+P2+(150*xCount),YOff+(150*yCount)),mask = temp)

                    if item["Count"] < 10:
                        MatNums.text((TXOff+P2+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                    else:
                        MatNums.text((TXOff+P2-4+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                        
                    xCount+=1

                if yCount<=4:                
                    xCount+=1
                Base.paste(StarBG,(XOff+P2+(150*3),YOff+(150*yCount)),mask = StarBG)
                if yCount==0:
                    MatNums.text((TXOff+P2-10+(150*3),YOff+25+(150*yCount)), str(TalNum+2),font = Bfont, fill=(0,0,0))
                else:
                    MatNums.text((TXOff+P2-15+(150*3),YOff+25+(150*yCount)), str(TalNum+2),font = Bfont, fill=(0,0,0))

                yCount+=1

            else:
                xCount = 0
                for item in rank[1]:
                    temp = os.path.join(UP,item["Id"].replace(" ","_")+".png")
                    temp = Image.open(temp).convert("RGBA")
                    Base.paste(temp,(XOff+P2+(150*xCount),YOff+(150*yCount)),mask = temp)

                    if item["Count"] < 10:
                        MatNums.text((TXOff+P2+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                    else:
                        MatNums.text((TXOff+P2-4+(150*xCount),TYOff+(150*yCount)), str(item["Count"]),font = font, fill=(0,0,0))
                        
                    xCount+=1
                Base.paste(StarBG,(XOff+P2+(150*4),YOff+(150*yCount)),mask = StarBG)
                MatNums.text((TXOff+P2-30+(150*4),YOff+25+(150*yCount)), str(TalNum+2),font = Bfont, fill=(0,0,0))

                yCount+=1

            TalNum+=1
            if TalNum == 5:
                yCount = 0
                P2 = (150*(xCount))
            if TalNum == 8:
                P2 = int((150*(xCount-1)))
        Base.paste(Panel,(XOff+P2,YOff+(150*yCount)),mask = Panel)

        Mats = list(CharMap[Char]["Normal Attack"]["Total"].keys())
        if Mats[0] in ["Teachings of Freedom","Teachings of Prosperity"]:
            Text = "Mon/Thur/Sun"
        elif Mats[0] in ["Teachings of Resistance","Teachings of Diligence"]:
            Text = "Tue/Fri/Sun"
        elif Mats[0] in ["Teachings of Ballad","Teachings of Gold"]:
            Text = "Wed/Sat/Sun"
        else:
            Text = "N/A"

        MatNums.text((TXOff+P2-30,YOff+25+(150*(yCount))), "Books : "+Text,font = Sfont, fill=(0,0,0))


        if Mats[6] in ["Dvalin's Plume","Dvalin's Sigh","Dvalin's Claw"]:
            Text = "Stormterror"
        elif Mats[6] in ["Tusk of Monoceros Caeli","Shard of a Foul Legacy","Shadow of the Warrior"]:
            Text = "Childe"

        elif Mats[6] in ["Tail of Boreas","Ring of Boreas","Spirit Locket of Boreas"]:
            Text = "Andrius/Wolf"
        else:
            Text = "N/A"
        MatNums.text((TXOff+P2-30,YOff+25+(150*(yCount+1))), "Special : "+Text,font = Sfont, fill=(0,0,0))
        Base.paste(Card,(-5,-100),mask = Card)
        Base.paste(Side,mask = Side)
        OP = os.path.join(F5,"Characters\\"+TagMap["Characters"][Char]["Name"]+"\\TAL.png")
        Base.save(OP)

        print(Char)

def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret

def StAdd(img):
    SBase = Image.new(mode = "RGBA" , size = (80,80) , color = (255,204,50))
    Star = XF.resize((80,80))
    SMask = Image.open(os.path.join(Earth, "BGC\\RStar.png")).convert("RGBA")
    Star.paste(SBase,mask = SMask)
    Star = Star.resize((32,32))    
    
    for item in ranger(0,4):
        img.paste(Star,(30+(38*item),64+205),mask = Star)


class WDetails:
    def __init__(self, name, rarity , sub , typ,icon):
        self.name = name
        self.rarity = rarity
        self.Substat_Type = sub
        self.Weapon_Type = typ
        self.icon = icon
        



Crest = Image.new(mode = "RGBA" , size = (337,256) , color = (0,0,0))

Fram = os.path.join(Earth, "BGC\\FinFrame.png")
Fram = Image.open(Fram).convert("RGBA")


class IFonts:
    Base_Atk_Value = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 48)
    Banner_Name = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 37)
    Class_SubType = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 23)
    WCard_Value_Font = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 27)
    Base_Atk = ImageFont.truetype(os.path.join(Earth, "Default_SC.ttf"), 25)

class Banners:
    #B# = [Banner Back,Frame Col]
    B5 = [Image.new(mode = "RGBA" , size = (555,66) , color = (188,105,50)),Image.new(mode = "RGBA" , size = (555,64) , color = (144,82,41))]
    B4 = [Image.new(mode = "RGBA" , size = (555,66) , color = (161,86,224)),Image.new(mode = "RGBA" , size = (555,64) , color = (125,69,173))]
    B3 = [Image.new(mode = "RGBA" , size = (555,66) , color = (81,128,203)),Image.new(mode = "RGBA" , size = (555,64) , color = (64,98,155))]
    B2 = [Image.new(mode = "RGBA" , size = (555,66) , color = (42,143,114)),Image.new(mode = "RGBA" , size = (555,64) , color = (35,110,89))]
    B1 = [Image.new(mode = "RGBA" , size = (555,66) , color = (114,119,138)),Image.new(mode = "RGBA" , size = (555,64) , color = (89,92,107))]

    def Cols(self,Rar):
#        print(Rar)
        if Rar == 5:
            return self.B5
        elif Rar == 4:
            return self.B4
        elif Rar == 3:
            return self.B3
        elif Rar == 2:
            return self.B2
        elif Rar == 1:
            return self.B1


def WCCreator(Contents,ValSet,Asc):
    BannerHei = 64
    XTPixOff = 32

    BCol = Banners().Cols(Contents.rarity)

    if Asc:
        TName = Contents.icon+"_Awaken #"
    else:
        TName = Contents.icon+" #"
    for item in os.listdir(F1):
        if TName in item:
            WeapImg = Image.open(os.path.join(F1,item)).convert("RGBA")

    Temp = os.path.join(Earth, "BGC\\"+str(Contents.rarity)+".png")
    Temp = Image.open(Temp).convert("RGBA")
    BGC = Temp.resize((337+218,BannerHei+256))
    BGC.paste(Weap_BG,(218,BannerHei),mask = Weap_BG)
    BGC.paste(WeapImg,(81+218,BannerHei),mask = WeapImg)
    StAdd(BGC)
    DatNums = ImageDraw.Draw(BGC)


    BGC.paste(BCol[0])
    BGC.paste(BCol[1],(0,1) ,mask = Fram)

    DatNums.text((32,14), Contents.name,font = IFonts.Banner_Name, fill=(255,255,255))
    DatNums.text((32,BannerHei+14), Contents.Weapon_Type,font = IFonts.Class_SubType, fill=(255,255,255))
    DatNums.text((32,BannerHei+47), Contents.Substat_Type,font = IFonts.Class_SubType, fill=(191,191,191))
    DatNums.text((32,BannerHei+74), str(ValSet[1]),font = IFonts.WCard_Value_Font, fill=(255,255,255))
    DatNums.text((32,BannerHei+115), "Base ATK",font = IFonts.Base_Atk, fill=(191,191,191))
    DatNums.text((32,BannerHei+142), str(ValSet[0]),font = IFonts.Base_Atk_Value, fill=(255,255,255))
    return BGC

def WeapCards():



    for Weapon in WeapMap:

        if TagMap["Weapons"][Weapon]["Ban"]:
            continue
        Weap = WeapMap[Weapon]

        Name = conv(Weap["Name"]).replace(" ","_")
        Rar = Weap["Rarity"]
#        print(Norm)
        SST = conv(Weap["Substat Type"])
        WT = Weap["Class"]
        if SST == "":
            Max = [Weap["Stats"][-1][1],"",Weap["Stats"][-1][0]]
            Min = [Weap["Stats"][1][1],"",Weap["Stats"][1][0]]
        elif SST == "Elemental Mastery":
            Max = [Weap["Stats"][-1][1],Weap["Stats"][-1][2],Weap["Stats"][-1][0]]
            Min = [Weap["Stats"][1][1],Weap["Stats"][1][2],Weap["Stats"][1][0]]
        else:
            Max = [Weap["Stats"][-1][1],str(Weap["Stats"][-1][2])+"%",Weap["Stats"][-1][0]]
            Min = [Weap["Stats"][1][1],str(Weap["Stats"][1][2])+"%",Weap["Stats"][1][0]]

        YTPixOff = 64
        BG = XF.resize((337+218,YTPixOff+256))
        BGB = ImageDraw.Draw(BG)
        BGB.rectangle((0, 0,337+218,YTPixOff+256), fill=(0,0,0))
    

        WBase = WDetails(conv(Weap["Name"]),Rar,conv(Weap["Substat Type"]),Weap["Class"],Weap["Icon"])
        #Base
#        BG = Image.new(mode = "RGBA" , size = (337+218,YTPixOff+256) , color = (0,0,0))
#        BG.paste(BGC,mask=BGC)
        BGC = WCCreator(WBase,Min,False)
        OP = ExCI(os.path.join(F5,"Weapons",Name),"BCard.png")
        BGC.save(OP)


        BGC = WCCreator(WBase,Max,True)

        OP = ExCI(os.path.join(F5,"Weapons",Name),"ACard.png")
        BGC.save(OP)


        #Combo


        ComboCard = Image.new(mode = "RGBA" , size = (337+218,2*(YTPixOff+256)))


        WBase.name = "Base Level - "+str(Min[2])
                
        ComboCard.paste(WCCreator(WBase,Min,False))

        WBase.name = "Max Level - "+str(Max[2])

        ComboCard.paste(WCCreator(WBase,Max,True),((0,YTPixOff+256)))

        OP = ExCI(os.path.join(F5,"Weapons",Name),"Combo.png")
        ComboCard.save(OP)

def FrameC():
    Temp = os.path.join(Earth, "BGC\\Frame.png")
    Temp = Image.open(Temp).convert("RGBA")
    Bas = XF.resize((555,70))
    LS = Temp.crop((1,0,32,70))
    RS = Temp.crop((33,0,63,70))
    Cen = Temp.crop((32,0,33,70))
    Cen = Cen.resize((494,70))
    Bas.paste(LS,mask = LS)#.resize(64,70)
    Bas.paste(Cen,(31,0),mask = Cen)#.resize(64,70)
    Bas.paste(RS,(525,0),mask = RS)
    Bas = Bas.resize((555,64))
    OP = ExCI(os.path.join(Earth, "BGC"),"FinFrame.png")
    Bas.save(OP)

def ImageTesting():
    Coating = Image.new(mode = "RGBA" , size = (555,66) , color = (161,86,224))
    FrameCoat = Image.new(mode = "RGBA" , size = (555,64) , color = (125,69,173))

    SBase = XF.resize((80,80))
    SInt = ImageDraw.Draw(SBase)
    SInt.rectangle((0, 0,80,80), fill=(255,204,50))
    Star = XF.resize((80,80))
    SMask = Image.open(os.path.join(Earth, "BGC\\RStar.png")).convert("RGBA")
    Star.paste(SBase,mask = SMask)
    Star = Star.resize((32,32))

    Temp = os.path.join(Earth, "BGC\\4.png")
    Temp = Image.open(Temp).convert("RGBA")

    Coating = XF.resize((555,70))
    CoatFrame = ImageDraw.Draw(Coating)
    CoatFrame.rectangle((0, 0,555,70), fill=(161,86,224))

    NCrest = XF.resize((337,256))
    Crest = Image.new(mode = "RGBA" , size = (337,256) , color = (0,0,0))

    Crest.paste(Crest,(218,65),mask = Weap_BG)
    Bands = Weap_BG.split()
    NN = ImageOps.invert(Bands[3])
#    NN.show()
    NN.save("C:\\Users\\eikea\\Documents\\Beyond Ultra\\Image Hell\\Earth\\BGC\\TestInverse.png")
    NBands = Image.merge('RGBA', (NN,NN,NN,NN))
#    NBands.show()
    NBands.save("C:\\Users\\eikea\\Documents\\Beyond Ultra\\Image Hell\\Earth\\BGC\\TestCompInverse.png")
    pixeldata = list(Weap_BG.getdata())
    Max = 0
    for i,pixel in enumerate(pixeldata):
        Pixel = pixeldata[i]
        if Pixel[3] > Max:
            Max = Pixel[3]
        pixeldata[i] = (Pixel[0],Pixel[1],Pixel[2],69-Pixel[3])
    Ni = Image.new(mode = "RGBA" , size = (337,256) )
    Ni.putdata(pixeldata)
#    Ni.show()
    Ni.save("C:\\Users\\eikea\\Documents\\Beyond Ultra\\Image Hell\\Earth\\BGC\\TestyTest.png")

    BGC = Temp.resize((337+218,65+256))
    BGC.paste(Crest,(218,65),mask = Ni)
    BGC.save("C:\\Users\\eikea\\Documents\\Beyond Ultra\\Image Hell\\Earth\\BGC\\TestyTestTesty.png")

#    BGC.show()

"""
Side = Image.open(Norm).convert("RGBA")


X1 = XF.resize((2048,1024))
X2 = X1.copy().convert("RGBA")

Norm = os.path.join(IO,item.replace(" ","_")+".png")
R4 = Image.open(Norm).convert("RGBA")
X2.paste(R4,mask = R4)
OP = os.path.join(UP,item+".png")
X2.save(OP)
"""


Fourth()
ICards()
CharAscIgs()
CharTalAscIgs()
CharImD()
FrameC()
WeapCards()
#ImageTesting()