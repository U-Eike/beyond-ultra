import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
langpath = os.path.join(Remapped_Data, "TextEN.json")
Itemdex = os.path.join(Out_Files, "ItemEN.json")
QMap = os.path.join(Bot_Files, "QuestMap.json")

langpath = os.path.join(Remapped_Data, "TextEN.json")

with open(langpath, encoding='utf-8') as temp:
    lang = json.load(temp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

QD = RMaploader("QuestData.json")
TD = RMaploader("TalkData.json")
MQD = RMaploader("MainQuestData.json")


def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret
    
def extractor():
    final = {}
    for item in MQD:
        Que = MQD[item]
        temp = {"Title" : Que["TitleTextMapHash"],
                "Desc" : Que["DescTextMapHash"],
                "Rewards" : Que["RewardIdList"],
                "Stages" : {},
        }
        if "Type" in Que:
            temp.update({"Type" : Que["Type"]})
        if "Series" in Que:
            temp.update({"Type" : Que["Series"]})
        final.update({Que["Id"] : temp})
    for item in QD:
        Que = QD[item]
        #if Que["MainId"] not in final:
        #    final.update({Que["MainId"] : {}})
        temp = {"Directive" : Que["DescTextMapHash"]}
        if item in TD:
            temp.update({"DiD" : item})
        else:
            temp.update({"DiD" : True})
        temp.update({"Diag" : item})
        final[Que["MainId"]]["Stages"].update({Que["Order"] : temp})



    with open(QMap, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def Main():
    extractor()

if __name__ == "__main__":
    Main()