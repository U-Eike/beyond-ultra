#######################
# Powershell XOR 2 Files
# xor.py
# Jul 2016
# Website: http://www.Megabeets.net
# Use: ./xor.py file1 file2 outputFile
# Example: ./xor.py C:\a.txt C:\b.txt C:\result.txt
#######################
import sys
import os
from pathlib import Path
os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or '.' 
inpath = os.path.join(mydir, 'In')
outpath = os.path.join(mydir, 'Out')

#1.1 xor 144
#1.2 xor 145
#0.9.x xor 151
key = 146




var = os.listdir(inpath)
live = 0
for file in var:
    fpath = inpath+"\\"+file
    opath = outpath+"\\"+file
    print(fpath)
#for file in Path(r"F:\csharp\PackedBinConverter\bin\Debug\net5.0\BinData").rglob("*.bin"):
    # Read files as byte arrays
    byte_array = bytearray(open(fpath, 'rb').read())

    # Set the length to be the smaller one
    size = len(byte_array)
    xord_byte_array = bytearray(size)

    # XOR between the files
    for i in range(size):
            xord_byte_array[i] = byte_array[i] ^ key

    # Write the XORd bytes to the output file	
    open(opath, 'wb').write(xord_byte_array)

##    print(file.stem + " xored")

