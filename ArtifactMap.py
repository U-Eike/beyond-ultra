import sys
import os
from pathlib import Path
import json
import math

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
langpath = os.path.join(mydir, "Lang")
datapath = os.path.join(mydir, "Base Data")
rpath = os.path.join(mydir, "Remapped Data")
RDP = os.path.join(Remapped_Data, "ReliquaryData.json")
Artm = os.path.join(Bot_Files, "ArtifMap.json")

#Artifact Files
def Baseloader(Loc):
    Loc = os.path.join(Base_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

RD = RMaploader("ReliquaryData.json")
RSD = RMaploader("ReliquarySetData.json")
EAD = RMaploader("EquipAffixData.json")
Rep = { "EQUIP_BRACER" : "Flower",
        "EQUIP_NECKLACE" : "Feather",
        "EQUIP_SHOES" : "Timepiece",
        "EQUIP_RING" : "Goblet",
        "EQUIP_DRESS" : "Headpiece"
}
def extractor():
    final = {}
    for item in RSD:
        temp = {}
        if item != "15000":
            setbon = {}
            count = 0
            for setstat in RSD[item]["SetNeedNum"]:
                AffixID = str(RSD[item]["EquipAffixId"])+str(count)
                setbon.update({setstat : [str(EAD[AffixID]["NameTextMapHash"]),str(EAD[AffixID]["DescTextMapHash"])]})
                temp.update({"Set Name" : str(EAD[AffixID]["NameTextMapHash"])})
                count+=1
            setpiece = {}
            for setpie in RSD[item]["ContainsList"]:
                setpiece.update({Rep[RD[str(setpie)]["EquipType"]] : [str(RD[str(setpie)]["NameTextMapHash"]),str(RD[str(setpie)]["DescTextMapHash"]),""]})
            temp.update({ "Set Bonuses" : setbon})
            temp.update({ "Set Pieces" : setpiece})





            final.update({item : temp})
    with open(Artm, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def Main():
    extractor()

if __name__ == "__main__":
    extractor()