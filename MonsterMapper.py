import sys
import os
from pathlib import Path
import json
import math
import re

os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Bot_Files = os.path.join(mydir, "Bot_Files")
langpath = os.path.join(Remapped_Data, "TextEN.json")
Itemdex = os.path.join(Out_Files, "ItemEN.json")
MonMap = os.path.join(Bot_Files, "MonMap.json")

langpath = os.path.join(Remapped_Data, "TextEN.json")

with open(langpath, encoding='utf-8') as temp:
    lang = json.load(temp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

IM = RMaploader("InvestigationMonster.json")
RP = RMaploader("RewardPreviewData.json")
DI = RMaploader("DisplayItemData.json")
MD = RMaploader("MaterialData.json")


def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret
    
def extractor():
    final = {}
    for item in IM:
        Mon = IM[item]
        temp = {}
        rewards = []
        for rew in RP[str(Mon["RewardPreviewId"])]["PreviewItems"]:
            if rew != {"Count": ""}:
                if rew["Id"] in [202]:
                    continue
                try:
                    rewards.append([rew["Id"],rew["Count"],DI[str(rew["Id"])]["NameTextMapHash"]])
                except:
                    if str(rew["Id"]) not in MD:
                        rewards.append([rew["Id"],rew["Count"],"Unknown"])
                        print(rew["Id"])
                    else:
                        rewards.append([rew["Id"],rew["Count"],MD[str(rew["Id"])]["NameTextMapHash"]])
        temp.update({"Rewards" :rewards})
        temp.update({"Name" :Mon["NameTextMapHash"]})
        temp.update({"Short" :Mon["DescTextMapHash"]})
        temp.update({"Icon" :Mon["Icon"]})
        temp.update({"Tier" :Mon["MonsterCategory"]})
        final.update({item : temp})

    with open(MonMap, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

extractor()