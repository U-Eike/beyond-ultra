import sys
import os
from pathlib import Path
import json
import math
from openpyxl import Workbook
import re


os.chdir(os.path.dirname(__file__))
mydir = os.path.dirname(__file__) or "."
Base_Data = os.path.join(mydir, "Base_Data")
Remapped_Data = os.path.join(mydir, "Remapped_Data")
Out_Files = os.path.join(mydir, "Out_Files")
Itemdex = os.path.join(Out_Files, "ItemEN.json")
langpath = os.path.join(Remapped_Data, "TextEN.json")
CharOut = os.path.join(Out_Files, "CharTables.json")
ChatOut = os.path.join(Out_Files, "RawDiag.json")
WeapOut = os.path.join(Out_Files, "WeaponTables.json")
NormOut = os.path.join(Out_Files, "NormalTables.json")
SkillOut = os.path.join(Out_Files, "SkillTables.json")
BurstOut = os.path.join(Out_Files, "BurstTables.json")

def ranger(base,max):
    ret = []
    for item in range(base,max):
        ret.append(item)
    return ret

def Baseloader(Loc):
    Loc = os.path.join(Base_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def RMaploader(Loc):
    Loc = os.path.join(Remapped_Data , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

def Outloader(Loc):
    Loc = os.path.join(Out_Files , Loc)
    with open(Loc, encoding='utf-8') as temp:
        NTemp = json.load(temp)
    return(NTemp)

AD = RMaploader("AvatarData.json")
APD = RMaploader("AvatarPromoteData.json")
ACD = RMaploader("AvatarCurveData.json")
lang = RMaploader("TextEN.json")
ASDD = RMaploader("AvatarSkillDepotData.json")
MTM = Outloader("ManualTextMap.json")
PSD = RMaploader("ProudSkillData.json")
ASD = RMaploader("AvatarSkillData.json")
WD = RMaploader("WeaponData.json")
WPD = RMaploader("WeaponPromoteData.json")
WCD = RMaploader("WeaponCurveData.json")
MD = RMaploader("MaterialData.json")
CRD = RMaploader("CookRecipeData.json")
CBD = RMaploader("CookBonusData.json")
DD = RMaploader("DialogueData.json")
TD = RMaploader("TalkData.json")
QCD = RMaploader("QuestCodexData.json")
ItemEN = Outloader(Itemdex)

def chartables():
    final = {}
    for item in AD:
        SNam = AD[item]["IconName"].split("_")[-1]
        
        BaseHP = AD[item]["HpBase"]
        HPCurve = AD[item]["PropGrowCurves"][0]["GrowCurve"]
        
        BaseATK = AD[item]["AttackBase"]
        ATKCurve = AD[item]["PropGrowCurves"][1]["GrowCurve"]
        
        BaseDEF = AD[item]["DefenseBase"]
        DEFCurve = AD[item]["PropGrowCurves"][2]["GrowCurve"]
                
        BaseCRate = AD[item]["Critical"]*100
        BaseCDmg = AD[item]["CriticalHurt"]*100
        Table = APD[str(AD[item]["AvatarPromoteId"])]
        Maxes = []
        for items in ranger(0,7):
            Maxes.append(Table[items]["UnlockMaxLevel"])
        
        HPBonuses = [0]
        for items in ranger(1,7):
            HPBonuses.append(Table[items]["AddProps"][0]["Value"])

        ATKBonuses = [0]
        for items in ranger(1,7):
            ATKBonuses.append(Table[items]["AddProps"][1]["Value"])

        DEFBonuses = [0]
        for items in ranger(1,7):
            DEFBonuses.append(Table[items]["AddProps"][2]["Value"])
        SPEType = lang[str(MTM[Table[0]["AddProps"][3]["PropType"]]["TextMapContentTextMapHash"])]

        SPEBonuses = [0]
        for items in ranger(1,7):
            try:
                if SPEType in ["Elemental Mastery"]:
                    SPEBonuses.append(math.trunc(Table[items]["AddProps"][3]["Value"]))
                else:
                    SPEBonuses.append(Table[items]["AddProps"][3]["Value"]*100)
            except:
                SPEBonuses.append(0)


        temp = [["Level","Base HP","Base ATK","Base DEF",SPEType]]
        Min = 0
        Bonus = 0
        HPBonus = HPBonuses[0]
        ATKBonus = ATKBonuses[0]
        DEFBonus = DEFBonuses[0]
        SPEBonus = SPEBonuses[0]
        for level in Maxes:

            index = Maxes.index(level)
            Max = level
            if index != 0:
                TempHP = math.floor(round(BaseHP,2)*round(ACD[HPCurve][Min-1],2)+HPBonuses[index])
                TempAtk = math.floor(round(BaseATK,2)*round(ACD[ATKCurve][Min-1],2)+ATKBonuses[index])
                TempDef = math.floor(round(BaseDEF,2)*round(ACD[DEFCurve][Min-1],2)+DEFBonuses[index])
                if SPEType == "CRIT DMG":
                    TempSpe = round(SPEBonuses[index]+BaseCDmg,1)
                elif SPEType == "CRIT Rate":
                    TempSpe = round(SPEBonuses[index]+BaseCRate,1)
                else:
                    TempSpe = round(SPEBonuses[index],1)

                temp.append([str(Min)+"+",TempHP,TempAtk,TempDef,TempSpe])
            for intitem in ranger(Min,Max):
                TempHP = math.trunc((BaseHP)*round(ACD[HPCurve][intitem],2)+HPBonuses[index])
                TempAtk = math.trunc(round(BaseATK,2)*round(ACD[ATKCurve][intitem],2)+ATKBonuses[index])
                TempDef = math.trunc(round(BaseDEF,2)*round(ACD[DEFCurve][intitem],2)+DEFBonuses[index])
                if SPEType == "CRIT DMG":
                    TempSpe = round(SPEBonuses[index]+BaseCDmg,1)
                elif SPEType == "CRIT Rate":
                    TempSpe = round(SPEBonuses[index]+BaseCRate,1)
                else:
                    TempSpe = round(SPEBonuses[index],1)
    
                temp.append([str(intitem+1),TempHP,TempAtk,TempDef,TempSpe])
            Min = Max
        temp2 = {"Stats" : temp}
        temp2.update({"Helper" : SNam})
        temp2.update({"BonusType" : SPEType})
        temp2.update({"Name" : AD[item]["NameTextMapHash"]})

        if item == "10000023":
            print("Here")

        final.update({item : temp2})

    with open(CharOut, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def normaltables():
    final = {}
    for unit in AD:
        SDID = str(AD[unit]["SkillDepotId"])
        GID = str(ASDD[SDID]["Skills"][0])
        try:
            TableID = str(ASD[GID]["ProudSkillGroupId"])
        except:
            final.update({unit : []})
            continue
        End = [[]]
        ValT = PSD[TableID]
        for level in ranger(0,15):
            Attrs = []
            PIDs = []
            Values = []
            Dets = []

            for Param in ValT[level]["ParamDescList"]:
                if lang[str(Param)] != "":
                    lang[str(Param)] = lang[str(Param)].replace("#{LAYOUT_MOBILE#Tapping}{LAYOUT_PC#Press}{LAYOUT_PS#Press}","Tap")
                    temp = lang[str(Param)].split("|")
                    Attrs.append(temp[0])
                    Values.append(temp[1])
                    PIDs.append(re.findall("{(.*?)}", temp[1]))
                    tloc = []
                    te = re.findall("{(.*?)}", temp[1])
                    for det in te:
                        tloc.append([int(det[5])-1,det.split(":")[1]])
                    Dets.append(tloc)

            for param in PIDs:
                Valdex = PIDs.index(param)
                for val in param:
                    Pardex = param.index(val)
                    Loc = Dets[Valdex][Pardex][0]
                    Typ = Dets[Valdex][Pardex][1]
                    #Amber/Fischl Skill Fix


                    if Typ == "P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100))+"%")

                    elif Typ == "F1":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc],1)))
                    elif Typ == "F1P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100,1))+"%")
                    elif Typ == "F2P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100,2))+"%")
                    elif Typ == "I":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc])))
                    elif Typ == "F":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100)))
                    else:
                        print(Typ)
                        print(ValT[level]["ParamDescList"][Loc])
                        print(Attrs[Valdex])
                        print(Values[Valdex])
                        print("")

            End.append(Values)
        End[0] = Attrs
        FTemp = {}
        FTemp.update({"Name" : ASD[GID]["NameTextMapHash"]})
        FTemp.update({"Description" : ASD[GID]["DescTextMapHash"]})
        FTemp.update({"Img" : ASD[GID]["SkillIcon"]})
        FTemp.update({"Table Data" : End})
        Evol = []
        Total = {}
        for level in ValT[1:10]:
            lv = level
            Tab = []
            for mat in lv["CostItems"]:
                if mat == {}:
                    continue
                if str(mat["Id"]) not in ItemEN:
                    Tab.append({"Id" : "Unknown","Count" : mat["Count"]})
                else:
                    Tab.append({"Id" : ItemEN[str(mat["Id"])],"Count" : mat["Count"]})
            Evol.append([lv["CoinCost"],Tab])

        for level in Evol:
            for mat in level[1]:
                if mat == {}:
                    continue
                if mat["Id"] in Total:
                    Total[mat["Id"]]+=mat["Count"]
                else:
                    Total.update({mat["Id"] : mat["Count"]})
        FTemp.update({"Total" : Total})
        FTemp.update({"Upgrade" : Evol})

        final.update({unit : FTemp})

    with open(NormOut, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def skilltables():
    final = {}
    for unit in AD:
        SDID = str(AD[unit]["SkillDepotId"])
        GID = str(ASDD[SDID]["Skills"][1])
        try:
            TableID = str(ASD[GID]["ProudSkillGroupId"])
        except:
            final.update({unit : []})
            continue
        End = [[]]
        ValT = PSD[TableID]
        for level in ranger(0,15):
            Attrs = []
            PIDs = []
            Values = []
            Dets = []

            for Param in ValT[level]["ParamDescList"]:
                if lang[str(Param)] != "":
                    lang[str(Param)] = lang[str(Param)].replace("#{LAYOUT_MOBILE#Tapping}{LAYOUT_PC#Press}{LAYOUT_PS#Press}","Tap")
                    temp = lang[str(Param)].split("|")
                    Attrs.append(temp[0])
                    Values.append(temp[1])
                    PIDs.append(re.findall("{(.*?)}", temp[1]))
                    tloc = []
                    te = re.findall("{(.*?)}", temp[1])
                    for det in te:
                        tloc.append([int(det[5])-1,det.split(":")[1]])
                    Dets.append(tloc)

            for param in PIDs:
                Valdex = PIDs.index(param)
                for val in param:
                    Pardex = param.index(val)
                    Loc = Dets[Valdex][Pardex][0]
                    Typ = Dets[Valdex][Pardex][1]
                    #Amber/Fischl Skill Fix


                    if Typ == "P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100))+"%")

                    elif Typ == "F1":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc],1)))
                    elif Typ == "F1P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100,1))+"%")
                    elif Typ == "F2P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100,2))+"%")
                    elif Typ == "I":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc])))
                    elif Typ == "F":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100)))
                    else:
                        print(Typ)
                        print(ValT[level]["ParamDescList"][Loc])
                        print(Attrs[Valdex])
                        print(Values[Valdex])
                        print("")

            End.append(Values)
        End[0] = Attrs
        FTemp = {}
        FTemp.update({"Name" : ASD[GID]["NameTextMapHash"]})
        FTemp.update({"Description" : ASD[GID]["DescTextMapHash"]})
        FTemp.update({"Img" : ASD[GID]["SkillIcon"]})
        FTemp.update({"Table Data" : End})
        Evol = []
        Total = {}
        for level in ValT[1:10]:
            lv = level
            Tab = []
            for mat in lv["CostItems"]:
                if mat == {}:
                    continue
                if str(mat["Id"]) not in ItemEN:
                    Tab.append({"Id" : "Unknown","Count" : mat["Count"]})
                else:
                    Tab.append({"Id" : ItemEN[str(mat["Id"])],"Count" : mat["Count"]})
            Evol.append([lv["CoinCost"],Tab])

        for level in Evol:
            for mat in level[1]:
                if mat == {}:
                    continue
                if mat["Id"] in Total:
                    Total[mat["Id"]]+=mat["Count"]
                else:
                    Total.update({mat["Id"] : mat["Count"]})
        FTemp.update({"Total" : Total})
        FTemp.update({"Upgrade" : Evol})


        final.update({unit : FTemp})

    with open(SkillOut, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def bursttables():
    final = {}
    for unit in AD:
        SDID = str(AD[unit]["SkillDepotId"])
        try:
            GID = str(ASDD[SDID]["EnergySkill"])
            TableID = str(ASD[GID]["ProudSkillGroupId"])
        except:
            final.update({unit : []})
            continue
        End = [[]]
        ValT = PSD[TableID]
        for level in ranger(0,15):
            Attrs = []
            PIDs = []
            Values = []
            Dets = []

            for Param in ValT[level]["ParamDescList"]:
                if lang[str(Param)] != "":
                    lang[str(Param)] = lang[str(Param)].replace("#{LAYOUT_MOBILE#Tapping}{LAYOUT_PC#Press}{LAYOUT_PS#Press}","Tap")
                    temp = lang[str(Param)].split("|")
                    Attrs.append(temp[0])
                    Values.append(temp[1])
                    PIDs.append(re.findall("{(.*?)}", temp[1]))
                    tloc = []
                    te = re.findall("{(.*?)}", temp[1])
                    for det in te:
                        tloc.append([int(det[5])-1,det.split(":")[1]])
                    Dets.append(tloc)

            for param in PIDs:
                Valdex = PIDs.index(param)
                for val in param:
                    Pardex = param.index(val)
                    Loc = Dets[Valdex][Pardex][0]
                    Typ = Dets[Valdex][Pardex][1]
                    #Amber/Fischl Skill Fix


                    if Typ == "P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100))+"%")

                    elif Typ == "F1":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc],1)))
                    elif Typ == "F1P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100,1))+"%")
                    elif Typ == "F2P":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100,2))+"%")
                    elif Typ == "I":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc])))
                    elif Typ == "F":
                        Values[Valdex] = Values[Valdex].replace("{"+PIDs[Valdex][Pardex]+"}",str(round(ValT[level]["ParamList"][Loc]*100)))
                    else:
                        print(Typ)
                        print(ValT[level]["ParamDescList"][Loc])
                        print(Attrs[Valdex])
                        print(Values[Valdex])
                        print("")

            End.append(Values)
        End[0] = Attrs

        FTemp = {}
        FTemp.update({"Name" : ASD[GID]["NameTextMapHash"]})
        FTemp.update({"Description" : ASD[GID]["DescTextMapHash"]})
        FTemp.update({"Img" : ASD[GID]["SkillIcon"]})
        FTemp.update({"Table Data" : End})
        Evol = []
        Total = {}
        for level in ValT[1:10]:
            lv = level
            Tab = []
            for mat in lv["CostItems"]:
                if mat == {}:
                    continue
                if str(mat["Id"]) not in ItemEN:
                    Tab.append({"Id" : "Unknown","Count" : mat["Count"]})
                else:
                    Tab.append({"Id" : ItemEN[str(mat["Id"])],"Count" : mat["Count"]})
            Evol.append([lv["CoinCost"],Tab])

        for level in Evol:
            for mat in level[1]:
                if mat == {}:
                    continue
                if mat["Id"] in Total:
                    Total[mat["Id"]]+=mat["Count"]
                else:
                    Total.update({mat["Id"] : mat["Count"]})
        FTemp.update({"Total" : Total})
        FTemp.update({"Upgrade" : Evol})



        final.update({unit : FTemp})


    with open(BurstOut, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def weapontables():
    final = {}
    c = []
    for item in WD:
        tcheck = WD[item]
        SNam = WD[item]["Icon"].split("_")[-1]
        
        BaseMain = WD[item]["WeaponProp"][0]["InitValue"]
        BaseCurve = WD[item]["WeaponProp"][0]["Type"]
        try:
            SubMain = WD[item]["WeaponProp"][1]["InitValue"]
            SubCurve = WD[item]["WeaponProp"][1]["Type"]
            SubType = str(MTM[WD[item]["WeaponProp"][1]["PropType"]]["TextMapContentTextMapHash"])
            RLev = WD[item]["WeaponProp"][1]["PropType"]
            if RLev not in ["FIGHT_PROP_ELEMENT_MASTERY"]:
                SubMain = SubMain*100

        except:
            SubMain = 0
            SubCurve = WD[item]["WeaponProp"][0]["Type"]
            SubType = "None"
            RLev = "None"
        Table = WPD[str(WD[item]["WeaponPromoteId"])]
        Maxes = []
        for items in ranger(0,len(Table)):
            Maxes.append(Table[str(items)]["UnlockMaxLevel"])
        
        MainBonuses = [0]
        for items in ranger(1,len(Table)):
            MainBonuses.append(Table[str(items)]["Value"])



        temp = [["Level","Main","Sub"]]
        Min = 0
        Bonus = 0
        MainBonus = MainBonuses[0]
        for level in Maxes:

            index = Maxes.index(level)
            Max = level
            for intitem in ranger(Min+1,Max+1):
                CurvValM = WCD[str(intitem)][BaseCurve]
                CurvValS = WCD[str(intitem)][SubCurve]
                TempMain = math.trunc((BaseMain*CurvValM)+MainBonuses[index])+1
                if RLev not in ["FIGHT_PROP_ELEMENT_MASTERY"]:
                    TempSub = round(SubMain*CurvValS,1)
                else:
                    TempSub = math.trunc(SubMain*CurvValS)
    
                temp.append([str(intitem),TempMain,TempSub])
            Min = Max
            if index != len(Table)-1:
                CurvValM = WCD[str(Min)][BaseCurve]
                CurvValS = WCD[str(Min)][SubCurve]
                TempMain = math.trunc((BaseMain*CurvValM)+MainBonuses[index+1])+1
                if RLev not in ["FIGHT_PROP_ELEMENT_MASTERY"]:
                    TempSub = round(SubMain*CurvValS,1)
                else:
                    TempSub = math.trunc(SubMain*CurvValS)

                temp.append([str(Min)+"+",TempMain,TempSub])


        FTemp = {}
        FTemp.update({"Type" : SubType})
        FTemp.update({"Stats" : temp})

        final.update({item : FTemp})

    with open(WeapOut, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def ilextractor():
    final = {}
    for item in MD:
        final.update({item : lang[str(MD[item]["NameTextMapHash"])]})
    with open("ItemEN.json", "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

FTyp = {"COOK_FOOD_ATTACK" : "1506899412","COOK_FOOD_DEFENSE" : "2585697844","COOK_FOOD_FUNCTION" : "526924292","COOK_FOOD_HEAL" : "339530060"}

def diver(Choi,merge):
    Poss = []
    for item in Choi:
        SubPoss = []
        SubPoss.append(["",item])
        Che = DD[str(item)]
        Nex = Che["NextDialogs"]
        while Nex != [] and Nex != [merge] and Nex != Choi:
            SubPoss.append(["",Nex[0]])
            Nex = DD[str(Nex[0])]["NextDialogs"]
        Poss.append(SubPoss)

    #print(Poss)
    return(Poss)


#I Hate Recursion So Imma Comment Here
#Nex = Next Dialog Set , Choi = Choices Passed To Reach This Point , Tree = Built Path So Far
def delver(Nex,Choi=[],Tree=[],PB=False,):
    #Base Overwrite
    #Base Case
    if Nex == [] or Nex in Choi or Nex in [[3760751],[10220346],[110120426],[112010209],[0]]:#Corrections [680000105->680000104]
        return Tree

    else:
        if len(Nex) == 1:
            if str(Nex[0]) not in DD:
                return Tree
            else:
                Tex = DD[str(Nex[0])]["NextDialogs"]
                return(Tree+delver(Tex,Choi+[Nex],[Nex[0]]))
        else:
            Pats = []
            for item in Nex:
                Tex = DD[str(item)]["NextDialogs"]
                Pats.append(delver(Tex,Choi+[Nex],[item]))
            PTest = []
            NPats = []
            for line in Pats:
                for item in line:
                    PTest.append(item)        
            RM = False
            for item in PTest:
                if PTest.count(item) == len(Nex):
                    ReMerge = item
                    #print(ReMerge)
                    RM = True
                    break
            if 3960732 in Nex:
                print("Here")
            if RM:
                Pats = []
                for item in Nex:
                    Tex = DD[str(item)]["NextDialogs"]
                    Pats.append(delver(Tex,Choi+[Nex]+[[ReMerge]],[item]))
            if RM:    
                if type(ReMerge) == int:
                    Tex = DD[str(ReMerge)]["NextDialogs"]
                return (Tree+[Pats]+delver(Tex,Choi+[Nex]+[ReMerge],[ReMerge]))
            else:
                return (Tree+[Pats])
            #print(len(Nex))
##            """
            """
            ReMerge = Nex
            Cha = False
            #print(len(NPats) == Pats)
  

            if ReMerge == "180000517":
                print(ReMerge)
            for item in Nex:
                Tex = DD[str(item)]["NextDialogs"]
                NPats.append(delver(Tex,Choi+[Nex]+[ReMerge],[item]))
            #print(NPats == Pats)
            """
            #return(delver(Tex,Choi+[Nex]+[ReMerge],Tree+Pats))

#            return Tree+NPats
##            """    
#            return(delver(Tex,Choi+[Nex],Tree+Pats))



def ender(Choi):
    Poss = []
    for item in Choi:
        Poss.append(item)
        Che = DD[str(item)]
        Nex = Che["NextDialogs"]
        while Nex != []:
            Poss.append(Nex[0])
            if str(Nex[0]) not in DD or Nex == Choi:
                Nex = []
            else:
                if len(Nex) == 1:
                    if Nex[0] in Choi:
                        Nex = []
                    else:
                        Nex = DD[str(Nex[0])]["NextDialogs"]
                elif len(Nex) > 1:
                    print(Nex)
                    Nex = DD[str(Nex[0])]["NextDialogs"]

    FinPoss = []
    for item in Poss:
        if Poss.count(item) == len(Choi):
            FinPoss.append(item)
    if len(FinPoss) == 0:
        for item in Poss:
            if Poss.count(item) == len(Choi)-1:
                FinPoss.append(item)
    try:
        return(FinPoss[0])
    except:
        return([])

def choice(Choi):
    if 3960702 in Choi:
        print("here")

    Remerge = ender(Choi)
    Branches = diver(Choi,Remerge)
    return([Remerge,Branches])

def chats():
    final = {}
    for item in TD:
        Diag = []
        BE = TD[item]
        if "InitDialog" not in BE:
            continue
        NT = [BE["InitDialog"]]
        
        if item == "34713":
            print("Here")
        if item == "1800005":
            print("Here")
        if str(NT[0]) not in DD:
            continue
        #print(item)
        Diag = delver(NT)
        #Decap Code
        """
        while NT != []:
            if len(NT) == 1:
                if str(NT[0]) not in DD:
                    break
                Tex = DD[str(NT[0])]
                Diag.append(str(NT[0]))
                if NT == Tex["NextDialogs"]:
                    break
                NT = Tex["NextDialogs"]
            elif len(NT) > 1:
                Diag.append(delver(NT))
                break
                Poss = choice(NT)
                Diag.append(Poss[1])
                if Poss[0] != []:
                    if Poss[0] == "3960702":
                        print("Here")
                    Tex = DD[str(Poss[0])]
                    if str(NT[0]) in Diag:
                        NT = []
                        break
                    Diag.append(str(NT[0]))
                    NT = Tex["NextDialogs"]
                else:
                    NT = []
                if 3960702 in NT:
                    print("here")
            else:
                Diag.append
                if NT == [3960702]:
                    print("Here")
#                 print(NT)
                break
        """
        final.update({item : Diag})

    with open(ChatOut, "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)

def RecMap():
    final = {}
    for item in CRD:
        temp = {}
        temp.update({"Helper" : lang[str(CRD[item]["NameTextMapHash"])]})
        temp.update({"Name" : str(CRD[item]["NameTextMapHash"])})
        temp.update({"Rarity" : str(CRD[item]["RankLevel"])})
        temp.update({"Type" : FTyp[CRD[item]["FoodType"]]})

        Materials = []
        for mat in CRD[item]["InputVec"]:
            if mat != {}:
                Materials.append([ItemEN[str(mat["Id"])],mat["Count"]])
        temp.update({"Materials" : Materials})
        
        Results = []
        for food in CRD[item]["QualityOutputVec"]:
            Results.append(str(food["Id"]))
        temp.update({"Variants" : Results})
        if item in CBD:
            temp.update({"Specialty" : [True,str(CBD[item]["AvatarId"])]})
            temp["Variants"].append(str(CBD[item]["ParamVec"][0]))
        else:
            temp.update({"Specialty" : [False,""]})

        final.update({item : temp})

    with open("Bot_Files//RecipeMapData.json", "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)


def QuestCodex():
    final = {}
    for item in QCD:
        Tree = QCD[item]
        temp = {}
        if Tree["ChapterId"] not in final:
            final.update({Tree["ChapterId"] : [Tree["ParentQuestId"]]})
        else:
            final[Tree["ChapterId"]].append(Tree["ParentQuestId"])
        

    with open("Bot_Files//QuestCodexTrees.json", "w", encoding='utf-8') as write_file:
        json.dump(final, write_file, indent=4, ensure_ascii=False)



#chats()
#print(delver([3960701]))
#End = delver([3960701])
#End = delver([3960702,3960707,3960715,3960727,3960741])
#diver([3471302, 3471303, 3471304],ender([708140706, 708140707, 708140708]))
def Main():
    chartables()
    normaltables()
    skilltables()
    bursttables()
    weapontables()
    RecMap()
    chats()

if __name__ == "__main__":
    QuestCodex()